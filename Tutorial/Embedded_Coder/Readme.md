# Embedded Coder

Embedded Coder es una herramienta que ofrece Matlab para la generación de código automático en C/C++. Esta herramienta cuenta con múltiples configuraciones que permiten una gran personalización del código generado, sin embargo, para poder generar el código de una manera correcta, es necesario tener en consideración algunas caracterísiticas que presenta esta herramienta.

## Consideraciones al momento de escribir código

### Formato Simulink para la generación de código

Pese al soporte que tiene Embedded Coder para la generación de código a partir de arhivos .m y diagramas Simulink, en la práctica, solo se aprecian cambios con respecto a Matlab Coder en los códigos generados en diagramas Simulink. En consecuencia, la primera consideración a tener en cuenta es que los códigos escritos en Matlab deben estar implementados en Simulink para poder realizar la generación de código con Embedded Coder. Adicionalmente, la generación de código solo puede realizarse en subsistemas dentro de nuestro diagrama de Simulink, es decir, si tenemos un modelo en donde queramos utilizar la herramienta, es necesario que este se encuentre en un subsistema para conseguir una generación de código exitosa. Ligado al punto anterior, si lo que queremos es tener la versión en C de una serie de funciones escritas en Matlab, es <b>completamente necesario</b> que cada una de estas funciones se encuentren en bloques diferentes, y que su generación debe ser cada una de las funciones de forma individual.

### Formato de lectura inputs-outputs

Dado que Matlab por defecto guarda las matrices por ccolumnas en su memoria, por defecto Embedded Coder guardara de la misma forma las matrices de entrada y de salida. Dad esta premisa es muy importante considerar que por defecto debemos de entregarle los argumentos de entrada y leer los argumentos de salida como columna, a menos que se defina diferente dentro de las opciones de Embedded Coder.

### Tamaño de variables

El tamaño de las variables dentro de un código en Matlab nunca fue un problema, es decir, si por alguna razón una variable cambia el tamaño que ocupa en memoria en algun ciclo for o algo similar, no presenta una dificultad ni algo que tener en consideración a la hora de ejecutar el código (sin considerar optimización de código o similares). Sin embargo, Embedded Coder no admite este tipo de ambigüedades, es decir, el tamaño de una variable, en lo que respecta al espacio en memoria que utiliza, es fija durante toda la ejecución del código. Entonces, si nuestro código en Matlab posee este tipo de situaciones, la generación de código no es soportada y deberá de modificarse el código.

### Parametros de entrada 

Ligado al punto anterior, el tamaño de las variables de entrada debe ser algo a tener en cuenta con Embedded Coder, ya que similar a como sería un programa escrito en C, las variables de entrada deben de tener especificado tanto el tipo como el tamaño que tendrán en caso de ser un arreglo. En consecuencia, se debe de especificar los tipos de variable de entrada, principalmente en tamaño para las funciones que se desee aplicar Embedded Coder.

Muy ligado al punto anterior, el tener parámetros de entrada opcionales puede llegar a generar problemas en el código generado, ya que, en algunos casos se puede omitir la inicialización de alguna variable iniciandola en algún valor no correspondido. En consecuencia, la variable no es considerada como una entrada en el código generado final, sino que, como una variable interna con un valor no deseado.

### Soporte de funciones

Pese a que Embedded Coder puede pasar la gran mayoría de códigos o modelos que escribamos en Matlab, tiene limitaciones de ciertas funciones propias de Matlab, no permitiendo la generación de código si nuestro script o modelo utiliza alguna de ellas. Las funciones que Embedded Coder soporta la generación de código puede verse en el siguiente [enlace.](https://la.mathworks.com/help/simulink/ug/functions-and-objects-supported-for-cc-code-generation.html).

# Flujo de trabajo

A continuación se muestra el flujo de trabajo sugerido para el uso correcto de la herramienta.

## Preparación del código

Como se mencionó en secciones anteriores, tener en consideración que puede y no puede llevar nuestro código es escencial para que el código generado tenga una funcionalidad equivalente. Entonces, debemos de tener nuestro modelo o función implementada en Simulink en un subsistema o bloque de función como se muestra en al figura 1. Luego dentro de Simulink, abrir el toolbox de Embedded Coder.

<center> 
    <img src="imagenes/embedded_modelo.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.1 - Configuración inicial de Embedded Coder.</b>
        </figcaption>
</center>

## Configuraciones 

Embedded Coder ofrece una cantidad de personalización bastante amplia, debido a que este trabajo de título se acota a realizar una validación y directrices iniciales del uso de esta herramienta, es que solo se mencionarán algunas de las configuraciones posibles dentro de nuestro código.

En primera instancia, tenemos que ir a las configuraciones de la herramienta y seleccionar las casillas de "Generate code only" y "Package code and artifacts" (figura 2), de esta forma el código final generado quedara empaquetado en un ZIP, el cual contendrá lo justo y necesario para poder correr el código. Posteriromente se debe de configurar el tipo de solver que se va a utilizar (figura 3) a un solver de paso fijo (fixed step). Luego, una de las partes mas importantes sería la prioridad que tendrá la herramienta con el código generado(figuras 4 y 5). Existen varias opciones para que la herramienta priorice, entre ellas la optimización en ejecución, en RAM, ROM, un código polyspace, entre otros, entonces dependiendo el tipo de aplicación o plataforma objetivo las prioridades podrían a llegar a ser diferentes. Finalmente la plataforma objetivo, en donde se puede seleccionar de antemano cual sera el procesador donde nuestro código sera implementado (figura 6), configurando de manera automática el tamaño en memoria de las variables.

<center> 
    <img src="imagenes/embedded_zip.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.2 - Configuración inicial de Embedded Coder.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_solver.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.3 - Configuración tipo de solver utilizado.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_objectives0.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.4 - Configuración de optimizaciones Embedded Coder.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_objectives.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.5 - Configuración de optimizaciones Embedded Coder.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_hardware.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.6 - Configuración de plataforma objetivo.</b>
        </figcaption>
</center>

## Generación de Código

Una vez con el modelo en Simulink listo con las configuraciones pertinentes, es conveniente abrir la generación de código guiada, dada por el botón "Quick Start" (figura 7). Los pasos a seguir quedan detallados de la figura 8 - 11, en donde incluye la selección del subsistema, si la generación de código será en C o C++, instancia única o múltiple, confirmación del procesador objetivo y optimización implementada. Se recomienda escoger los siguientes parámetros:
- Generación de código en C.
- Instancia única.
- optimización en ejecución.


<center> 
    <img src="imagenes/embedded_quickStart.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.7 - Generación de código guiada.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_quickStart2.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.8 - Selección del subsistema para la generación de código.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_quickStart3.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.9 - Selección de instancia única y lenguaje.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_proce.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.10 - Selección de procesador objetivo.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/embedded_optimizacion.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.11 - Selección de tipo de optmización.</b>
        </figcaption>
</center>


## Creación de archivo main

Siguiendo el flujo de trabajo como se mencionó con anterioridad para la generación de código, se obtendrían 2 funciones importantes, **NombreFuncion_step()** y **NombreFuncion_initialize()**.  Ambas funciones son las principales para que nuestro código funcione como estamos esperando. Por un lado, **NombreFuncion_step()** se encarga de realizar el algoritmo o ecuaciones que constituyan un modelo, y por otro **NombreFuncion_initialize()** se encarga de darle valor a las variables de entrada. En caso de un algoritmo bastaría con darle valor a las variables de entrada una vez, sin embargo, en el caso de los modelos, esta asignación debe de realizarse en cada paso de simulación. 

Cabe destacar que la función **NombreFuncion_initialize()** debe de editarse de manera manual, ya que inicialmente viene vacía. Adicionalmente, las entradas quedan definidas en una estructura denominada **rtU** y las salidas en una estructura llamada **rtY**, ambas estructuras respetarían las entradas y salidas definidas en Matlab.

Entonces, tendríamos dos formas de confecionar el archivo main, uno considerando un algoritmo, y otro considerando un modelo. Comenzando por el main que considera que es un algoritmo:

```
int main(){
    NombreFuncion_initialize();
    NombreFuncion_step();
    return 0;
}
```

Luego para el caso de un modelo:

```
int main(){
    for (int i = 0; i < cant_pasos; i++>){
        NombreFuncion_initialize();
        NombreFuncion_step();
    }
    return 0;
}
```

## Compilación e implementación

Finalmente solo nos restaría la compilación e implemención de los códigos generados, dependiendo del tipo de plataforma y sistema operativo, la implementación puede variar. En general la implementación en un microcontrolador con RTOS o Bare Metal esta explicado en la seción de [STM32](/Tutorial/STM32/), en consecuencia, la implementación más común vendría a ser su compilación en un sistema operativo de Linux, la cual, puede variar dependiendo del tipo de código empleado, ya que por ejemplo para el caso de estudio del ADMM, es encesario incluir la flag <b>-lm</b> para su correcta compilación. Finalmente bastaria con compilar los archivos .c más alguna flag requerida:

```
gcc main.c NombreFuncion.c -lm 
```

Y con respecto a la ejecución, sería la misma que cualquier programa en C.

```
./nombre_ejecutable.out
```

