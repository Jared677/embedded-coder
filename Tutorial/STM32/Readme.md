# Configuración STM32F7xx

Para realizar la implementación y validación de los códigos generados por Embedded Coder en las tarjetas de la familia STM32f7xx es necesario realizar algunas configuraciones en el IDE utilizado para programarlas (STM32 CubeIDE). Debido a la naturaleza del caso de estudio y las limitaciones que nos presenta trabajar con esta tarjeta, es que es necesario realizar configuraciones para la comunicación con el computador de escritorio, quien mandará y recibira datos, siendo las entradas y resultados respectivamente, para su procesamiento y validación. Adicionalmente, configuraciones con el tipo de datos que se utilizaran y tipo de sistema operativo (Bare Metal o RTOS) también son mostradas.

## Configuración comunicación

Por lo general la mayoria tarjetas de esta familia traen habilitado un módulo de comunicación por USART por defecto, sin embargo, cuando creemos un proyecto no hay certeza que este módulo se encuentre habilitado por defecto, además los pines que puede tener asignado para en la configuración inicial podrían no ser los correctos. Para el caso de este proyecto, la tarjeta utilizada son la STM32F767 y STTM32F411, las cuales tiene asignados los pines <b>PD8</b>, <b>PD9</b> y <b>PB6</b>, <b>PB7</b> para la comunicación a través de USB con un computador de escritorio. En la figuras 1 se puede apreciar las configuraciones necesarias para tener una comunicación sin problemas con un computador de escritorio vía USB.

<center> 
    <img src="imagenes/USART1.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.1 - Configuraciones de pines del módulo USART.</b>
        </figcaption>
</center>

## Configuración punto flotante

Para poder hacer uso de números que no sean enteros en la comunicación, es necesario habilitar una opción dentro del IDE <b> Proyecto > Propiedades > MCU Settings > Use float with printf from newlib-nano (-u_printf_float) </b> (figuras 3 y 4), para de esta forma asegurarnos de poder compilar el código que involucre comunicación con números que no sean enteros.

<center> 
    <img src="imagenes/float1.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.2 - Propiedades de nuestro proyecto.</b>
        </figcaption>
    <img src="imagenes/float2.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.3 - Configuración utilización punto flotante.</b>
        </figcaption>
</center>

## Configuración RTOS

Con las configuraciones por defecto del IDE, se estaría programando en Bare Metal la tarjeta. Para poder utilizar RTOS es necesario crear un proyecto y configurarlo como muestra la figura 5.
<center> 
    <img src="imagenes/rtos1.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.4 - Configuración freeRTOS.</b>
        </figcaption>
</center>

Trabajar con freeRTOS es bastante simple en esta familia de tarjetas debido al IDE utilizado. Debido a que la finalidad de trabajar con freeRTOS es dar garantías temporales, es que el uso de un reloj adecuado en el microcontrolador. Utilizar el reloj interno no es la mejor idea, sino que, utilizar el un módulo de timers, donde bastara con escoger cualquiera mientras pertenezca a este módulo.

<center> 
    <img src="imagenes/option_sys.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.5 - Configuración reloj que usará freeRTOS 1.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/select_timer.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.6 - Configuración reloj que usará freeRTOS 2.</b>
        </figcaption>
</center>


Ahora bien, RTOS trabajan con tareas o hilos para realizar sus procesos. Es posible crear a partir del IDE las tareas que vamos a utilizar, especificando prioridad, tamaños en el stack, nombres, etc. De esta forma no hay que preocuparse por crearlas posteriormente de manera "manual". Sin embargo, es bueno tener una idea de como es la sintaxis que tiene crear una, ya que, de esta forma podemos hacer cambios sin necesidad de recurrir a las configuraciones del IDE:

```
/* Creacion de los atributos de la tarea y tarea en si*/

osThreadId_t Nombre_tarea_Handle;
const osThreadAttr_t Nombre_tarea_attributes = {
  .name = "Nombre_tarea",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};

/* Nombre funcion que efecutara la tarea */
void Start_Nombre_tarea(void *argument);
```
Luego en el archivo main, antes de iniciar el scheduler se asigna los parametros y funcionalidad a la tarea:

```
Nombre_tarea_Handle = osThreadNew(StartTask02, NULL, &Nombre_tarea_attributes);
```


# Caractrización de tiempo de ejecución

Para poder caracterizar el tiempo de ejecución, es necesario activar el contador de ciclos, para de esta forma saber cuanto tiempo esta ocupado el procesador en una tarea en base a la cantidad de ciclos invertidos en esta. Cabe destacar que la razón por la cual se midió el tiempo así, es para tener una mayor precisión en los valores obtenidos, ya que con funciones como <b>HAL_tick()</b> no llegan a la presición de nanosegundos.
La sintaxis entonces es la siguiente:

```
CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
DWT->CYCCNT = 0;
DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
```

En caso de tener problemas debemos de modificar el registro LAR, ya que uno de los problemas más comúnes de porque no funciona la activación anterior son por temas de autenticación (para mas información consultar el [manual](https://developer.arm.com/documentation/ddi0403/latest/) sección B2.3.1)

```
DWT->LAR = 0xC5ACCE55; 
```

Entonces la forma de medir el tiempo de ejecución es el siguiente:

```
t1 = DWT->CYCCNT; // TIC

/* DO STUFF */

t2 = DWT->CYCCNT; // TOC

diff = t2 - t1; //elpased time in cycles

```

# Comunicación con USART

La forma que tiene esta tarjeta para comunicarse de forma serial (USART), es utilizando la biblioteca HAL, principalmente usando las siguientes funciones para mandar y recibir datos:

```
HAL_UART_Receive(&huart3, (uint8_t *)Array, largo_total_recibido_en_bits, tiempo_espera_en_ms);
HAL_UART_Transmit(&huart3, (uint8_t *)Array, largo_total_enviado_en_bits, tiempo_espera_en_ms);
```
Debido a que esta comunicación permite enviar solo de a 1 byte con un tipo de dato específico (uint8_t), es que es necesario castear desde un arreglo los datos que queremos enviar. 

Como alternativa para la recepción y envío de datos, se propone mandar el número como una cadena de char (de un char a la vez) de tal forma que al recepcionarlo se obtenga un string que pueda ser posteriormente convertido a un valor float. Análogamente, mandar los números como una cadade char para ser recepcionados tal y posteriormente convertidos en float. Por ejemplo, el número 1278 sería enviado como '1','2','7' y '8', recepcionandolo de la misma forma, obteniendo '1278' como un string para ser convertido en un float de 1278. 

# Creación de archivo Main RTOS vs Bare Metal

Debido a la forma en que se programa cuando tenemos o no RTOS, es necesario hacer una distinción en como es que se crea el main para cada situación. Para el caso de Bare Metal basta con crear un <b> while(1) </b> dentro de la función main y allí hacer llamado al código generado:

```
int main(void) {
    /* do stuff */
    while(1) {
        /* do stuff */
        Codigo_generado_initialize();
	    Codigo_generado_step();
        /* do stuff */
    }
}

```

Ahora para el caso de RTOS es ligeramente diferente:

```
int main(void) {
    /* do stuff */

    /* Init scheduler */
    osKernelInitialize();

    /* creacion nombre_tarea
    Nombre_tarea_Handle = osThreadNew(StartTask02, NULL, &Nombre_tarea_attributes);

    /* Start scheduler */
    osKernelStart();

    while(1) {
    }
}

```