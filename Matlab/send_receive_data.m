clear;
clc;
%% Variable init
s = serialport("COM7", 115200);
n = input("Dimension del problema: ");
fileID = fopen("ADMM\input_simplify"+n+".txt", 'r');
P_input(n*n) = [0];
A_input(n*n) = [0];
rho_input = 0;
Array_solucion(n*3) = [0];
Array_supremo_sol = [0];
i = 0;
k = 1;
cont = 1;
flag = 1;




%% LOOP

while i < 10000
    cont = 1;
    % Read txt file
    P_input  = split(fgetl(fileID), ' ');
    A_input  = split(fgetl(fileID), ' ');
    rho_input  = split(fgetl(fileID), ' ');
    Array_to_send = [P_input', A_input', rho_input'];
    Array_to_send = Array_to_send(~cellfun('isempty',Array_to_send));
    
    for k = 1:length(Array_to_send)
    % Send inputs to ST
        write(s,1,"uint8"); %flag to init communication
        write(s, string(Array_to_send(cont)), 'string');
        cont = cont + 1;
        pause(0.1);
    end
    % Receive and store data frrom ST
%     for k = 1:n 
%         sol_x(i+1,k) = readline(s);
%         sol_u(i+1,k) = readline(s);
%         sol_z(i+1,k) = readline(s);
%     end
    
    % Receive process time
    time(i+1) = readline(s);

    
    i = i + 1;

end

% Close txt file
fclose(fileID);

disp("done")

