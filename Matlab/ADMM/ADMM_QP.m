function [x, z, u] = ADMM_QP(P, q, A, b, init_x, init_z, init_u, rho, iters)
   
    % ------ DATA INITIALIZATION ------ %
    % Length of variables
    x = init_x;
    z = init_z;
    u = init_u;
    n = size(q,1);
    R = P + rho*(A'*A);
    matrix = [R zeros(n,n)];
    % Inverse Matrix Calculation - Gauss Jordan Method
    
     for i = 1:n 
         for j = 1:(2*n)
             if j == (i+n)
                 matrix(i,j) = 1;
             end
         end
     end
     
     for i = (n-1):1
         if (matrix(i-1,0) < matrix(i,0)) 
            temp = matrix(i);
            matrix(i) = matrix(i - 1);
            matrix(i - 1) = temp;
         end
     end
 			
     for i = 1:n 
         for j = 1:n
             if j ~= i
                 temp = matrix(j,i) / matrix(i,i);
                 for k = 1:(2*n)
                     matrix(j,k) = matrix(j,k) - matrix(i,k) * temp;
                 end
             end
         end
     end
 
 	 	
     for i = 1:n
        temp = matrix(i,i);
        for j = 1:(2*n)
           matrix(i,j) = matrix(i,j)/temp; 
        end
     end
 	 	
 	R_inv =  matrix(:,n+1:end);
     % Iterations
     for k = 1:iters
         v_x = z - b + u;
         x = R_inv * (-rho*A'*v_x - q);    % update x
            
         z = max(0, -A*x - u + b);             % update z
 
         % Then we update the scaled dual variable
         u = u + (A*x + z - b);                % update u
     end

end

