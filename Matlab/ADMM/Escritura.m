
seed  = rng(1);

%% Iterative ADMM
N = input("Ingrese la cantidad de pruebas que desea generar: ");  % total valid set of outputs 
m = input("Dimension: ");
objval = zeros(N,1);    % Vector for ADMM's objective values
err = zeros(N,1);       % Vector for ADMM's error (compared to quadprog) 
i = 0; %counter 
input_name = "input_simplify" + m + ".txt";
output_name = "output_simplify" + m + ".txt";
fileID0 = fopen(input_name,'w+');
fileID = fopen(output_name,'w+');
% N Iterations for ADMM
while true
    if i == N 
        break; 
    end
    % Generate problem data
    ADMM_params = generate_problem_data(m);
    
    % Compute the solution for the above problem data
    [init_ADMM_x, init_ADMM_z, init_ADMM_u]=ADMM_QP(ADMM_params.P,ADMM_params.q, ...
       ADMM_params.A, ADMM_params.b, ADMM_params.x, ...
       ADMM_params.z, ADMM_params.u, ADMM_params.rho, 50); %se agrego el opt_rho
    
    if  (abs(init_ADMM_x) < 100000 ) & (abs(init_ADMM_z) < 100000) & (abs(init_ADMM_u) < 100000) %this conditions to get valid outputs
    % Write outputs of algorithm in output_simplify.txt
        fprintf(fileID, '%f ',  init_ADMM_x); %x
        fprintf(fileID, '\n');
        fprintf(fileID, '%f ',  init_ADMM_z); %z
        fprintf(fileID, '\n');
        fprintf(fileID, '%f ',  init_ADMM_u); %u
        fprintf(fileID, '\n');
    % Write inputs of algorithm in input_simplify.txt
        fprintf(fileID0, '%f ',  ADMM_params.P); %P
        fprintf(fileID0, '\n');
        fprintf(fileID0, '%f ',  ADMM_params.A); %A
        fprintf(fileID0, '\n');
        fprintf(fileID0, '%f ', ADMM_params.rho); %rho
        fprintf(fileID0, '\n');
     % Count a valid input-output
        i = i + 1;
    end
end

fclose(fileID);
fclose(fileID0);

%% Generate problem data
function ADMM_params = generate_problem_data(m)
    % Problem
    % Min: 1/2*x^T*P*x + q^T*x + g(z)
    % ST:   Ax + z = b
    % g(z) is the indicator function of Z:
        % g(z) = 0 if z in Z,
        % g(z) = ∞ if any component of z not in Z
    %
    % P -> R^(n x n)
    % A -> R^(n x n)
    % q -> R^(n)
    % c -> R^(n)
    % x -> R^(n)
    % z -> R^(n)
    
    
    
    % Data initialization
    ADMM_params.x = zeros(m,1);
    ADMM_params.z = zeros(m,1);
    ADMM_params.u = zeros(m,1);
    
    
    for k = 1:m
        ADMM_params.q(k,1) = (4*k)/m;
        ADMM_params.b(k,1) = (4*k)/m;
    end
    % generate a well-conditioned positive definite matrix
    % (for faster convergence)
    P = randn(m);
    P = P + P';
    [V, ~] = eig(P);
    ADMM_params.P = V*diag(1+randn(1,m))*V';
    A = randn(m);
    A = A + A';
    [VA, ~] = eig(A);
    ADMM_params.A = VA*diag(1+randn(1,m))*VA';
    ADMM_params.rho = rhof(ADMM_params.A, ADMM_params.P); 
    
    

    
end