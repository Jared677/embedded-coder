function rho=rhof(A,Q)
    [~,S,V] = svds(A);
    Sd=(S'*S)^(-1/2);
    Pd=Sd'*V'*Q*V*Sd;
    E=eig(Pd);
    beta1=1/(E(1));
    beta2=1/(max(E));
    a4=beta1*beta2*(beta1^2+beta2^2);
    a3=beta1^3+beta2^3-3*beta1*beta2*(beta1+beta2);
    a2=(beta1-beta2)^2;
    a1=2*(beta1+beta2);
    a0=2;
    root=roots([a4,-a3, -a2,-a1,-a0]);
    rho=abs(root(root>0));
    rho = rho(1);
end