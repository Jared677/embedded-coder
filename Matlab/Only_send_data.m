clear;
clc;
%% Inicializacion variables
s = serialport("COM10", 115200);
fileID = fopen("ADMM\input_simplify.txt", 'r');
N = input("Ingrese cantidad de entradas a enviar: ");
P_input(16) = [0];
A_input(16) = [0];
rho_input = 0;
Array_solucion(12) = [0];
Array_supremo_sol = [0];


%% LOOP

while i < N
    cont = 1;
    % Write txt file
    P_input  = split(fgetl(fileID), ' ');
    A_input  = split(fgetl(fileID), ' ');
    rho_input  = split(fgetl(fileID), ' ');
    Array_to_send = [P_input', A_input', rho_input'];
    Array_to_send = Array_to_send(~cellfun('isempty',Array_to_send));
    
    for k = 1:length(Array_to_send)
        % Send data 
        write(s,1,"uint8");
        write(s, string(Array_to_send(cont)), 'string');
        cont = cont + 1;
        pause(0.1);
    end

end

% Close txt file
fclose(fileID);

disp("done")
toc
