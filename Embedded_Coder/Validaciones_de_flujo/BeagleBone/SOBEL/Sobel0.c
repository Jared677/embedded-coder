/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Sobel0.c
 *
 * Code generated for Simulink model 'Sobel0'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Wed Aug 31 19:04:01 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "Sobel0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Forward declaration for local functions */
static void sum(const real_T x[9], real_T y[3]);

/* Function for MATLAB Function: '<Root>/Sobel' */
static void sum(const real_T x[9], real_T y[3])
{
  int32_T xi;
  int32_T xpageoffset;
  for (xi = 0; xi < 3; xi++) {
    xpageoffset = xi * 3;
    y[xi] = x[xpageoffset + 2] + (x[xpageoffset + 1] + x[xpageoffset]);
  }
}

/* Model step function */
void Sobel0_step(void)
{
  real_T Mx[9];
  real_T x[3];
  real_T Gx;
  real_T Gy;
  int32_T Mx_tmp;
  int32_T Mx_tmp_0;
  int32_T i;
  int32_T i_0;
  int32_T j;
  static const int8_T Mx_0[9] = { -1, -2, -1, 0, 0, 0, 1, 2, 1 };

  static const int8_T My[9] = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };

  /* Outport: '<Root>/filtered_image' incorporates:
   *  MATLAB Function: '<Root>/Sobel'
   */
  memset(&rtY.filtered_image[0], 0, 250000U * sizeof(real_T));

  /* MATLAB Function: '<Root>/Sobel' incorporates:
   *  Inport: '<Root>/input_image'
   *  Outport: '<Root>/filtered_image'
   */
  for (i = 0; i < 498; i++) {
    for (j = 0; j < 498; j++) {
      for (i_0 = 0; i_0 < 3; i_0++) {
        Mx_tmp = (i_0 + j) * 500 + i;
        Mx[3 * i_0] = rtU.input_image[Mx_tmp] * (real_T)Mx_0[3 * i_0];
        Mx_tmp_0 = 3 * i_0 + 1;
        Mx[Mx_tmp_0] = rtU.input_image[Mx_tmp + 1] * (real_T)Mx_0[Mx_tmp_0];
        Mx_tmp_0 = 3 * i_0 + 2;
        Mx[Mx_tmp_0] = rtU.input_image[Mx_tmp + 2] * (real_T)Mx_0[Mx_tmp_0];
      }

      sum(Mx, x);
      Gx = (x[0] + x[1]) + x[2];
      for (i_0 = 0; i_0 < 3; i_0++) {
        Mx_tmp = (i_0 + j) * 500 + i;
        Mx[3 * i_0] = rtU.input_image[Mx_tmp] * (real_T)My[3 * i_0];
        Mx_tmp_0 = 3 * i_0 + 1;
        Mx[Mx_tmp_0] = rtU.input_image[Mx_tmp + 1] * (real_T)My[Mx_tmp_0];
        Mx_tmp_0 = 3 * i_0 + 2;
        Mx[Mx_tmp_0] = rtU.input_image[Mx_tmp + 2] * (real_T)My[Mx_tmp_0];
      }

      sum(Mx, x);
      Gy = (x[0] + x[1]) + x[2];
      rtY.filtered_image[(i + 500 * (j + 1)) + 1] = sqrt(Gx * Gx + Gy * Gy);
    }
  }
}

/* Model initialize function */
void Sobel0_initialize(float Array[250000])
{
  for (int k = 0; k < 250000; k++) {
      rtU.input_image[k] = Array[k];
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
