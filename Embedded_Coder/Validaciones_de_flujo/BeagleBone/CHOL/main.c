#include <stdio.h>
#include <stddef.h>
#include "MATLAB0.h"
#include <stdlib.h>
#include "time.h"
#include "math.h"

struct timespec diff(struct timespec start, struct timespec end);

static float Array[100] = {0};

int main() {
    FILE *input;
    FILE *output;
    FILE *uwu;
    input = fopen("/home/debian/definitivo/validaciones/cholesky/input.txt", "r");
    output = fopen("/home/debian/definitivo/validaciones/cholesky/output.txt", "r");
    uwu = fopen("results.txt", "w+");
    struct timespec start, stop, difference;

    int N = 25; //input size
    int M = 25; //output size
    char buff[32] = {0};
    double accum = 0;
    float nisan = 0;
    float otosan  = 0;
    int error = 0;
    float tmp = 0;

    if (input == NULL) {
        printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
        return 0;
    }
    if (output == NULL) {
        printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
        return 0;
    }
    if (uwu == NULL) {
        printf("ERROR EN ASIGNACION EN EL PUNTERO DE uwu\n");
        return 0;
    }
    for (int i = 0; i < N; i++) {
        fscanf(input, "%s", buff);
        //printf("input nro %d : %f\n",i,atof(buff));
        Array[i] = atof(buff);
    }

    MATLAB0_initialize(Array);
    clock_gettime(CLOCK_REALTIME, &start); //tic
    MATLAB0_step();
    clock_gettime(CLOCK_REALTIME, &stop); //toc
    difference = diff(start, stop);
    accum = difference.tv_nsec;

    for (int k = 0; k < M; k++) {
        fscanf(output, "%s", buff);
        otosan = atof(buff) - rtY.y[k];
        nisan = fabs(otosan);
        if (fabs(nisan/rtY.y[k]) > fabs(tmp)){tmp = nisan/rtY.y[k];}
        if (fabs(rtY.y[k]) < 0.00001) {rtY.y[k] = 0;}
        if ( nisan > fabs(atof(buff)) * 0.01) {
            error++;
        }
    }

    for (int i = 0; i < M; i++){
        fprintf(uwu, "%f\n", rtY.y[i]);
    }

    fclose(input);
    fclose(output);
    fclose(uwu);

    printf("Error maximo: %f %\n",tmp*100);
    printf("Errores totales: %d \n Tiempo empleado: %lf[us]\n",error,accum/1000);

    return 0;
}

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
