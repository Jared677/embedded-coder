/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Broyden0.h
 *
 * Code generated for Simulink model 'Broyden0'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Wed Aug 24 17:20:53 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Broyden0_h_
#define RTW_HEADER_Broyden0_h_
#include <math.h>
#ifndef Broyden0_COMMON_INCLUDES_
#define Broyden0_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* Broyden0_COMMON_INCLUDES_ */

/* Model Code Variants */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T init_x[2];                    /* '<Root>/init_x' */
  real_T init_jacobian[4];             /* '<Root>/init_jacobian' */
} ExtU;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T xr[2];                        /* '<Root>/xr' */
} ExtY;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY rtY;

/* Model entry point functions */
extern void Broyden0_initialize(void);
extern void Broyden0_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('broyden_nolin/Broyden')    - opens subsystem broyden_nolin/Broyden
 * hilite_system('broyden_nolin/Broyden/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'broyden_nolin'
 * '<S1>'   : 'broyden_nolin/Broyden'
 */
#endif                                 /* RTW_HEADER_Broyden0_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
