/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Broyden0.c
 *
 * Code generated for Simulink model 'Broyden0'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Wed Aug 24 17:20:53 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "Broyden0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Forward declaration for local functions */
static void mldivide(const real_T A[4], const real_T B_0[2], real_T Y[2]);

/* Function for MATLAB Function: '<Root>/Broyden' */
static void mldivide(const real_T A[4], const real_T B_0[2], real_T Y[2])
{
  real_T Y_tmp;
  real_T a21;
  int32_T r1;
  int32_T r2;
  if (fabs(A[1]) > fabs(A[0])) {
    r1 = 1;
    r2 = 0;
  } else {
    r1 = 0;
    r2 = 1;
  }

  a21 = A[r2] / A[r1];
  Y_tmp = A[r1 + 2];
  Y[1] = (B_0[r2] - B_0[r1] * a21) / (A[r2 + 2] - Y_tmp * a21);
  Y[0] = (B_0[r1] - Y_tmp * Y[1]) / A[r1];
}

/* Model step function */
void Broyden0_step(void)
{
  real_T L[4];
  real_T U[4];
  real_T br[4];
  real_T frxskt[4];
  real_T sk[2];
  real_T tmp[2];
  real_T f_idx_0;
  real_T f_idx_1;
  real_T rtb_xr_idx_0;
  real_T rtb_xr_idx_1;
  real_T temp;
  int32_T i;
  int32_T iy;
  int8_T ipiv_idx_0;
  int8_T perm_idx_0;
  int8_T perm_idx_1;
  boolean_T exitg1;

  /* MATLAB Function: '<Root>/Broyden' incorporates:
   *  Inport: '<Root>/init_jacobian'
   *  Inport: '<Root>/init_x'
   */
  br[0] = rtU.init_jacobian[0];
  br[1] = rtU.init_jacobian[1];
  br[2] = rtU.init_jacobian[2];
  br[3] = rtU.init_jacobian[3];
  rtb_xr_idx_0 = rtU.init_x[0];
  rtb_xr_idx_1 = rtU.init_x[1];
  i = 0;
  exitg1 = false;
  while ((!exitg1) && (i < 1000)) {
    frxskt[0] = br[0];
    frxskt[1] = br[1];
    frxskt[2] = br[2];
    frxskt[3] = br[3];
    ipiv_idx_0 = 1;
    iy = 0;
    if (fabs(br[1]) > fabs(br[0])) {
      iy = 1;
    }

    if (br[iy] != 0.0) {
      if (iy != 0) {
        ipiv_idx_0 = 2;
        frxskt[0] = br[1];
        frxskt[1] = br[0];
        frxskt[2] = br[3];
        frxskt[3] = br[2];
      }

      frxskt[1] /= frxskt[0];
    }

    if (frxskt[2] != 0.0) {
      frxskt[3] += frxskt[1] * -frxskt[2];
    }

    perm_idx_0 = 1;
    perm_idx_1 = 2;
    if (ipiv_idx_0 > 1) {
      perm_idx_1 = 1;
      perm_idx_0 = 2;
    }

    L[0] = 0.0;
    L[1] = 0.0;
    U[1] = 0.0;
    L[2] = 0.0;
    U[2] = 0.0;
    L[3] = 0.0;
    U[3] = 0.0;
    U[0] = frxskt[0];
    for (iy = 0; iy < 2; iy++) {
      U[iy + 2] = frxskt[iy + 2];
    }

    L[perm_idx_0 - 1] = 1.0;
    L[perm_idx_1 - 1] = frxskt[1];
    sk[0] = -((rtb_xr_idx_0 + rtb_xr_idx_1) - 1.0);
    L[perm_idx_1 + 1] = 1.0;
    sk[1] = -((rtb_xr_idx_1 * rtb_xr_idx_1 * 4.0 + rtb_xr_idx_0 * rtb_xr_idx_0)
              - 4.0);
    mldivide(L, sk, tmp);
    mldivide(U, tmp, sk);
    rtb_xr_idx_0 += sk[0];
    rtb_xr_idx_1 += sk[1];
    if ((fabs(0.0 - rtb_xr_idx_0) < 0.01) && (fabs(1.0 - rtb_xr_idx_1) < 0.01))
    {
      exitg1 = true;
    } else {
      f_idx_0 = (rtb_xr_idx_0 + rtb_xr_idx_1) - 1.0;
      f_idx_1 = (rtb_xr_idx_1 * rtb_xr_idx_1 * 4.0 + rtb_xr_idx_0 * rtb_xr_idx_0)
        - 4.0;
      temp = sk[0] * sk[0] + sk[1] * sk[1];
      br[0] += f_idx_0 * sk[0] / temp;
      br[1] += f_idx_1 * sk[0] / temp;
      br[2] += f_idx_0 * sk[1] / temp;
      br[3] += f_idx_1 * sk[1] / temp;
      i++;
    }
  }

  /* Outport: '<Root>/xr' */
  rtY.xr[0] = rtb_xr_idx_0;
  rtY.xr[1] = rtb_xr_idx_1;
}

/* Model initialize function */
void Broyden0_initialize(void)
{
  rtU.init_x[0] = 1;
  rtU.init_x[1] = 2;
  rtU.init_jacobian[0] = 1;
  rtU.init_jacobian[1] = 2;
  rtU.init_jacobian[2] = 2;
  rtU.init_jacobian[3] = 16;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
