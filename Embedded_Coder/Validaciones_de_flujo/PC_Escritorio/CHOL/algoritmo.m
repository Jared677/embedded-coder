rng(100)
rows = 5;
cols = 5;
A = 4*randn(rows,cols);
A = A*A';
f1 = fopen("input.txt","w+");
for k = 1 : cols
    for j = 1 : rows
        fprintf(f1, '%f',  A(j,k));
        fprintf(f1, '\n');
    end
end
% for k = 1 : rows
%     for j = 1 : cols
%         fprintf(f1, '%f',  B(k,j));
%         fprintf(f1, '\n');
%     end
% end
fclose(f1);

%algoritmo
C = chol(A);

f0 = fopen("output.txt","w+");
for k = 1 : cols
    for j = 1 : rows
        fprintf(f0, '%f ',  C(j,k));
        fprintf(f0, '\n');
    end
end
fclose(f0);



