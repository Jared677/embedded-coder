/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Wed Aug 31 19:16:29 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MATLAB0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T rtb_y[25];
  real_T c;
  real_T ssq;
  int32_T b_ix;
  int32_T b_iy;
  int32_T b_j;
  int32_T b_k;
  int32_T d;
  int32_T i;
  int32_T ia;
  int32_T idxA1j;
  int32_T idxAjj;
  int32_T ix;
  boolean_T exitg1;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   */
  memcpy(&rtb_y[0], &rtU.A[0], 25U * sizeof(real_T));
  i = 0;
  b_j = 1;
  exitg1 = false;
  while ((!exitg1) && (b_j - 1 < 5)) {
    idxA1j = (b_j - 1) * 5;
    idxAjj = (idxA1j + b_j) - 1;
    ssq = 0.0;
    if (b_j - 1 >= 1) {
      b_ix = idxA1j;
      b_iy = idxA1j;
      for (b_k = 0; b_k <= b_j - 2; b_k++) {
        ssq += rtb_y[b_ix] * rtb_y[b_iy];
        b_ix++;
        b_iy++;
      }
    }

    ssq = rtb_y[idxAjj] - ssq;
    if (ssq > 0.0) {
      ssq = sqrt(ssq);
      rtb_y[idxAjj] = ssq;
      if (b_j < 5) {
        if (b_j - 1 != 0) {
          b_ix = idxAjj + 5;
          b_iy = ((4 - b_j) * 5 + idxA1j) + 6;
          for (b_k = idxA1j + 6; b_k <= b_iy; b_k += 5) {
            ix = idxA1j;
            c = 0.0;
            d = (b_k + b_j) - 2;
            for (ia = b_k; ia <= d; ia++) {
              c += rtb_y[ia - 1] * rtb_y[ix];
              ix++;
            }

            rtb_y[b_ix] += -c;
            b_ix += 5;
          }
        }

        ssq = 1.0 / ssq;
        idxA1j = ((4 - b_j) * 5 + idxAjj) + 6;
        for (idxAjj += 5; idxAjj + 1 <= idxA1j; idxAjj += 5) {
          rtb_y[idxAjj] *= ssq;
        }
      }

      b_j++;
    } else {
      rtb_y[idxAjj] = ssq;
      i = b_j;
      exitg1 = true;
    }
  }

  if (i == 0) {
    i = 4;
  } else {
    i -= 2;
  }

  for (b_j = 0; b_j <= i; b_j++) {
    for (idxAjj = b_j + 1; idxAjj < i + 1; idxAjj++) {
      rtb_y[idxAjj + 5 * b_j] = 0.0;
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* Outport: '<Root>/y' */
  memcpy(&rtY.y[0], &rtb_y[0], 25U * sizeof(real_T));
}

/* Model initialize function */
void MATLAB0_initialize(float Array[25])
{
    for (int k = 0; k < 25; k++) {
        rtU.A[k] = Array[k];
    }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
