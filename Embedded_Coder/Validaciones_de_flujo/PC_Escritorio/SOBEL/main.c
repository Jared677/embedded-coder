#include <stdio.h>
#include <stddef.h>
#include "Sobel0.h"
#include <stdlib.h>
#include "time.h"
#include "math.h"

struct timespec diff(struct timespec start, struct timespec end);
static float Array[250000] = {0};


int main() {
    FILE *input;
    FILE *output;
    FILE *uwu;
    input = fopen("D:\\A_IMPORTANTE\\Clion_projects\\Sobel\\Miko.txt", "r");
    output = fopen("D:\\A_IMPORTANTE\\Clion_projects\\Sobel\\Miko_sobel.txt", "r");
    uwu = fopen("results.txt", "w+");
    struct timespec start, stop, difference;

    int N = 250000;
    char buff[32] = {0};
    double accum = 0;
    float nisan = 0;
    float otosan  = 0;
    int error = 0;

    if (input == NULL) {
        printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
        return 0;
    }
    if (output == NULL) {
        printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
        return 0;
    }
    for (int i = 0; i < N; i++) {
        fscanf(input, "%s", buff);
        Array[i] = atof(buff);
    }

    Sobel0_initialize(Array);
    clock_gettime(CLOCK_REALTIME, &start); //tic
    Sobel0_step();
    clock_gettime(CLOCK_REALTIME, &stop); //toc
    difference = diff(start, stop);
    accum = difference.tv_nsec;

    for (int k = 0; k < N; k++) {
        fscanf(output, "%s", buff);
        otosan = atof(buff) - rtY.filtered_image[k];
        nisan = fabs(otosan);
        if (fabs(rtY.filtered_image[k] < 0.00001)){rtY.filtered_image[k] = 0;}
        if ( nisan > fabs(atof(buff)) * 0.01) {
            error++;
        }
    }

    for (int i = 0; i < N; i++) {
        fprintf(uwu, "%f\n", rtY.filtered_image[i]);
    }

    fclose(input);
    fclose(output);
    fclose(uwu);

    //printf("errores: %d\n",error);
    printf("Errores totales: %d \n Tiempo empleado: %lf[us]\n",error,accum/1000);

    return 0;
}

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
