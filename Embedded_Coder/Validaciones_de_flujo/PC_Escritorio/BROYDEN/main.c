#include <stdio.h>
#include "Broyden0.h"
#include "time.h"

struct timespec diff(struct timespec start, struct timespec end);

int main() {

    float output[2] = {-0.0021,1.0036};
    float array_error[2] = {0};
    struct timespec start, stop, difference;
    double accum = 0;
    Broyden0_initialize();

    clock_gettime( CLOCK_REALTIME, &start); //tic
    Broyden0_step();
    clock_gettime( CLOCK_REALTIME, &stop); //toc

    difference = diff(start, stop);
    accum = difference.tv_nsec;

    for (int k = 0; k < 2; k++) {
        array_error[k] = 100*fabs(output[k] - rtY.xr[k])/fabs(output[k]);
    }

    for (int k = 0; k < 2; k++) {
        printf("El numero obtenido fue %f, el esperado era %f y el error obtenido fue de %f\n", rtY.xr[k], output[k],array_error[k]);
    }
    printf("Tiempo empleado de: %lf [us]",accum/1000);

    return 0;
}

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
