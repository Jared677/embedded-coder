function nisan(br,xr,f,xsol,error_max,frxskt)
for i=1:1000
    f(1)=xr(1)+xr(2)-1;
    f(2)=xr(1)^2+4*xr(2)^2-4;
    [L,U]=lu(br);
    xold=xr;
    sk= U \(L\-f);
    xr=xold+sk;
    error1=abs(xsol(1)-xr(1));
    error2=abs(xsol(2)-xr(2));
    if error1<error_max && error2<error_max
        break
    end
    %fold=f;
    f(1)=xr(1)+xr(2)-1;
    f(2)=xr(1)^2+4*xr(2)^2-4;
    %yr=f-fold;
    sktsk=sk(1)^2+sk(2)^2;
    for j=1:2
        for k=1:2
            frxskt(j,k)=f(j)*sk(k);
        end
    end
    bsk=frxskt/sktsk;
    bold=br;
    br=bold+bsk;
end
end