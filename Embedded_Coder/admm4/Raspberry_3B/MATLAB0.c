/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Thu May 12 14:51:32 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MATLAB0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T matrix[32];
  real_T matrix_tmp[16];
  real_T matrix_tmp_0[16];
  real_T tmp[16];
  real_T rtb_u[4];
  real_T rtb_x[4];
  real_T rtb_z[4];
  real_T rtb_z_idx_1;
  real_T rtb_z_idx_2;
  real_T rtb_z_idx_3;
  real_T temp;
  int32_T i;
  int32_T k;
  int32_T matrix_tmp_1;
  int32_T matrix_tmp_tmp;
  int32_T matrix_tmp_tmp_tmp;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   *  Inport: '<Root>/P'
   *  Inport: '<Root>/b'
   *  Inport: '<Root>/init_u'
   *  Inport: '<Root>/init_x'
   *  Inport: '<Root>/init_z'
   *  Inport: '<Root>/iters'
   *  Inport: '<Root>/q'
   *  Inport: '<Root>/rho'
   */
  for (i = 0; i < 4; i++) {
    rtb_x[i] = rtU.init_x[i];
    rtb_z[i] = rtU.init_z[i];
    rtb_u[i] = rtU.init_u[i];
    matrix_tmp_tmp = i << 2;
    matrix_tmp[matrix_tmp_tmp] = rtU.A[i];
    matrix_tmp[matrix_tmp_tmp + 1] = rtU.A[i + 4];
    matrix_tmp[matrix_tmp_tmp + 2] = rtU.A[i + 8];
    matrix_tmp[matrix_tmp_tmp + 3] = rtU.A[i + 12];
  }

  for (k = 0; k < 4; k++) {
    for (i = 0; i < 4; i++) {
      matrix_tmp_tmp_tmp = k << 2;
      matrix_tmp_tmp = i + matrix_tmp_tmp_tmp;
      matrix_tmp_0[matrix_tmp_tmp] = 0.0;
      matrix_tmp_0[matrix_tmp_tmp] += rtU.A[matrix_tmp_tmp_tmp] * matrix_tmp[i];
      matrix_tmp_0[matrix_tmp_tmp] += rtU.A[matrix_tmp_tmp_tmp + 1] *
        matrix_tmp[i + 4];
      matrix_tmp_0[matrix_tmp_tmp] += rtU.A[matrix_tmp_tmp_tmp + 2] *
        matrix_tmp[i + 8];
      matrix_tmp_0[matrix_tmp_tmp] += rtU.A[matrix_tmp_tmp_tmp + 3] *
        matrix_tmp[i + 12];
    }
  }

  for (k = 0; k < 16; k++) {
    matrix[k] = rtU.rho * matrix_tmp_0[k] + rtU.P_f[k];
    matrix[k + 16] = 0.0;
  }

  for (i = 0; i < 4; i++) {
    for (matrix_tmp_tmp = 0; matrix_tmp_tmp < 8; matrix_tmp_tmp++) {
      if (matrix_tmp_tmp + 1 == i + 5) {
        matrix[i + (matrix_tmp_tmp << 2)] = 1.0;
      }
    }
  }

  for (i = 0; i < 4; i++) {
    for (matrix_tmp_tmp = 0; matrix_tmp_tmp < 4; matrix_tmp_tmp++) {
      if (matrix_tmp_tmp != i) {
        temp = matrix[(i << 2) + matrix_tmp_tmp] / matrix[(i << 2) + i];
        for (k = 0; k < 8; k++) {
          matrix_tmp_tmp_tmp = k << 2;
          matrix_tmp_1 = matrix_tmp_tmp_tmp + matrix_tmp_tmp;
          matrix[matrix_tmp_1] -= matrix[matrix_tmp_tmp_tmp + i] * temp;
        }
      }
    }
  }

  for (i = 0; i < 4; i++) {
    temp = matrix[(i << 2) + i];
    for (matrix_tmp_tmp = 0; matrix_tmp_tmp < 8; matrix_tmp_tmp++) {
      matrix_tmp_tmp_tmp = (matrix_tmp_tmp << 2) + i;
      matrix[matrix_tmp_tmp_tmp] /= temp;
    }
  }

  if (0 <= (int32_T)rtU.iters - 1) {
    for (k = 0; k < 16; k++) {
      tmp[k] = -rtU.A[k];
    }
  }

  for (i = 0; i < (int32_T)rtU.iters; i++) {
    temp = (rtb_z[0] - rtU.b[0]) + rtb_u[0];
    rtb_z_idx_1 = (rtb_z[1] - rtU.b[1]) + rtb_u[1];
    rtb_z_idx_2 = (rtb_z[2] - rtU.b[2]) + rtb_u[2];
    rtb_z_idx_3 = (rtb_z[3] - rtU.b[3]) + rtb_u[3];
    for (k = 0; k < 4; k++) {
      rtb_z[k] = (((matrix_tmp[k + 4] * -rtU.rho * rtb_z_idx_1 + -rtU.rho *
                    matrix_tmp[k] * temp) + matrix_tmp[k + 8] * -rtU.rho *
                   rtb_z_idx_2) + matrix_tmp[k + 12] * -rtU.rho * rtb_z_idx_3) -
        rtU.q[k];
    }

    for (k = 0; k < 4; k++) {
      rtb_x[k] = matrix[k + 28] * rtb_z[3] + (matrix[k + 24] * rtb_z[2] +
        (matrix[k + 20] * rtb_z[1] + matrix[k + 16] * rtb_z[0]));
    }

    for (matrix_tmp_tmp = 0; matrix_tmp_tmp < 4; matrix_tmp_tmp++) {
      temp = rtb_u[matrix_tmp_tmp];
      rtb_z_idx_1 = fmax(0.0, ((((tmp[matrix_tmp_tmp + 4] * rtb_x[1] +
        tmp[matrix_tmp_tmp] * rtb_x[0]) + tmp[matrix_tmp_tmp + 8] * rtb_x[2]) +
        tmp[matrix_tmp_tmp + 12] * rtb_x[3]) - temp) + rtU.b[matrix_tmp_tmp]);
      rtb_z[matrix_tmp_tmp] = rtb_z_idx_1;
      rtb_u[matrix_tmp_tmp] = (((((rtU.A[matrix_tmp_tmp + 4] * rtb_x[1] +
        rtU.A[matrix_tmp_tmp] * rtb_x[0]) + rtU.A[matrix_tmp_tmp + 8] * rtb_x[2])
        + rtU.A[matrix_tmp_tmp + 12] * rtb_x[3]) + rtb_z_idx_1) -
        rtU.b[matrix_tmp_tmp]) + temp;
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* Outport: '<Root>/x' */
  rtY.x[0] = rtb_x[0];

  /* Outport: '<Root>/z' */
  rtY.z[0] = rtb_z[0];

  /* Outport: '<Root>/u' */
  rtY.u[0] = rtb_u[0];

  /* Outport: '<Root>/x' */
  rtY.x[1] = rtb_x[1];

  /* Outport: '<Root>/z' */
  rtY.z[1] = rtb_z[1];

  /* Outport: '<Root>/u' */
  rtY.u[1] = rtb_u[1];

  /* Outport: '<Root>/x' */
  rtY.x[2] = rtb_x[2];

  /* Outport: '<Root>/z' */
  rtY.z[2] = rtb_z[2];

  /* Outport: '<Root>/u' */
  rtY.u[2] = rtb_u[2];

  /* Outport: '<Root>/x' */
  rtY.x[3] = rtb_x[3];

  /* Outport: '<Root>/z' */
  rtY.z[3] = rtb_z[3];

  /* Outport: '<Root>/u' */
  rtY.u[3] = rtb_u[3];
}


/* Model initialize function */
void MATLAB0_initialize(float array[32])
{

  for(int j = 0; j < 33; j++) {
	  if (j < 16) {
		  rtU.P_f[j] = array[j];
		  //printf("P[%d]: %f\n",j, rtU.P_d[j]);
	  } else if (j < 32) {
		  rtU.A[j - 16] = array[j];
		  //printf("A[%d]: %f\n",j-16, rtU.A[j-16]);
	  } else {
		  rtU.rho = array[j];

		  //printf("RHO: %f\n", rtU.rho_input);
	  }

  }
  rtU.iters = 3;
  for (int j = 0; j < 4; j++) {
	  rtU.q[j] = j + 1;
	  rtU.b[j] = j + 1;
	  rtU.init_x[j] = 0;
	  rtU.init_z[j] = 0;
	  rtU.init_u[j] = 0;
  }

}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
