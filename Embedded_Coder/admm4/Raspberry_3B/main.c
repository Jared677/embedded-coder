/*
 * main.c
 *
 *  Created on: 11-05-2022
 *      Author: Jared
 */


/*
 * CMAKE_CXX_FLAGS
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}${GCC_COVERAGE_COMPILE_FLAGS}")
*/


#include <stdio.h>

#include <stdlib.h>
#include "MATLAB0.h"
#include "rtwtypes.h"
#include<time.h>


#define BILLION  1000000000L;
//void validation (void);

struct timespec diff(struct timespec start, struct timespec end);


int main() {

	/* Variable initialization */
	FILE *input;
	FILE *output;
	FILE *time;

//    struct timeval st, et;
//    clock_t before = clock();
//    clock_t difference = 0;
//    clock_t acumulado = 0;

	struct timespec start, stop;


	float Array[33];
	char buff[32];
	int N = 100000; //99975
	int M = 100;
	double error_acumulado = 0;
	double entire_loops = 0;




    time = fopen("/home/pi/Desktop/VERSIONES/default/time.txt","w+");
    /* loop del loop */
    for (int w = 0; w < M; w++) {

        input = fopen("/home/pi/Desktop/VERSIONES/default/input_simplify.txt","r");
        output = fopen("/home/pi/Desktop/VERSIONES/default/output_simplify.txt","r");
        int error_x = 0;
        int error_z = 0;
        int error_u = 0;
        //int cursed_var = 0; //NOSE porque pero sin esta var no funciona el programa

        int tmp = 0;
        int err = 0;

        if (input==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
            return 0;
        }
        if (output==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
            return 0;
        }
        struct timespec difference;
        double accum = 0;
        /* LOOP tests */
        for (int k = 0; k < N; k++) {



            for (int i = 0; i < 33; i++) {
                fscanf(input, "%s", buff);
                Array[i] = atof(buff);
            }


            MATLAB0_initialize(Array);

            //before = clock();
            //gettimeofday(&st,NULL);
            clock_gettime( CLOCK_REALTIME, &start);
            MATLAB0_step();
            clock_gettime( CLOCK_REALTIME, &stop);

            difference = diff(start, stop);
            accum = difference.tv_nsec;
            entire_loops = entire_loops + accum;
            //accum = ( stop.tv_nsec - start.tv_nsec );// estaba dividido por 1000 y casteado como double


            fprintf(time, "%lf" "%s", accum, "\n");
            //gettimeofday(&et,NULL);
            //difference = clock() - before;

            //acumulado = difference + acumulado;

            for (int j = 0; j < 12; j++) {
                fscanf(output, "%s", buff);

                if (j < 4) {
                    //printf("X: %f, X_txt: %f\n", rtY.x[j], atof(buff));
                    if (fabs(rtY.x[j] - atof(buff)) > fabs(atof(buff)*0.01) && (rtY.x[j] != 0 && atof(buff) != 0)) {
                        error_x++;
                        tmp++;
                        error_acumulado = error_acumulado + fabs(rtY.x[j] - atof(buff)); 
                        /*printf("El valor obtenido en X fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), fabs(atof(buff)*0.01));
    */				}

                } else if (j < 8) {
                    //printf("Z: %f, Z_txt: %f\n", rtY.z[j - 4], atof(buff));
                    if (fabs(rtY.z[j - 4] - atof(buff)) > fabs(atof(buff)*0.01) && (rtY.z[j - 4] != 0 && atof(buff) != 0)) {
                        error_z++;
                        tmp++;
                        error_acumulado = error_acumulado + fabs(rtY.z[j] - atof(buff));
                        /*printf("El valor obtenido en Z fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);*/
                    }

                } else {
                    //printf("U: %f, U_txt: %f\n", rtY.u[j - 8], atof(buff));
                    if ((fabs(rtY.u[j - 8] - atof(buff)) > fabs(atof(buff)*0.01)) && (rtY.u[j - 8] != 0 && atof(buff) != 0)) {
                        error_u++;
                        tmp++;
                        error_acumulado = error_acumulado + fabs(rtY.u[j] - atof(buff));
                        /*printf("El valor obtenido en U fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);*/
                        }

                }
            }
            if (tmp != 0){
                err++;
                tmp = 0;
            }
        }


            /*printf("X : %f, %f, %f, %f\n",rtY.x[0], rtY.x[1], rtY.x[2], rtY.x[3]);
            printf("Z : %f, %f, %f, %f\n",rtY.z[0], rtY.z[1], rtY.z[2], rtY.z[3]);
            printf("U : %f, %f, %f, %f\n",rtY.u[0], rtY.u[1], rtY.u[2], rtY.u[3]);*/

        //acumulado = (double)1000000*acumulado;
        //total = (float)((error_x + error_z + error_u )/ 4);

        //printf("Errores totales en test %d: %d\n", w, err);
        //printf("Errores en x: %d\nErrores en z: %d\nErrores en u: %d\n", error_x, error_z, error_u);
        //printf("El error acumulado fue de : %lf \n", error_acumulado);


        //printf("tiempo empleado: %f", (double)acumulado / CLOCKS_PER_SEC);
        //double elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec);
        //printf("Sorting time: %f micro seconds\n",elapsed);


        //printf( "%lf\n", accum );


        /* Close txt */
        fclose(input);
        fclose(output);
    }
    printf("En todos los loops se demora un total de : %lf segundos\n", entire_loops/1000000000);
    printf("DONE\n");
    fclose(time);
	return 0;
}

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

