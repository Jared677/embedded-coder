/*
 * main.c
 *
 *  Created on: 11-05-2022
 *      Author: Jared
 */



#include <stdio.h>

#include <stdlib.h>
#include "MATLAB0.h"
#include "rtwtypes.h"
#include<time.h>



struct timespec diff(struct timespec start, struct timespec end);

double rasp3B = 5973289;
double rasp4B = 2123644;
double beaglebone = 18607440;
int raspberry3_counter, raspberry4_counter, beaglebone_counter = 0;

int main() {

	/* Variable initialization */
	FILE *input;
	FILE *output;
    FILE *time;
    struct timespec start, stop;
	float Array[33];
	char buff[32];
	int N = 100000;
    int W = 100;
    double tolerance = 0.001;
    double accum_error = 0;
    double entire_loops = 0;

    time = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\untitled\\time.txt","w+");
    /* Repetition tests LOOP */
    for (int w = 0; w < W; w++) {

        input = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\untitled\\input_simplify2.txt","r");
        output = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\untitled\\output_simplify2.txt","r");
        int error_x = 0;
        int error_z = 0;
        int error_u = 0;

//        int tmp = 0;
//        int err = 0;

        if (input==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
            return 0;
        }
        if (output==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
            return 0;
        }
        if (time==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
            return 0;
        }
        struct timespec difference;
        double accum = 0;
        /* LOOP tests */
        for (int k = 0; k < N; k++) {

            for (int i = 0; i < 33; i++) {
                fscanf(input, "%s", buff);
                Array[i] = atof(buff);
            }


            MATLAB0_initialize(Array);

            clock_gettime( CLOCK_REALTIME, &start);
            MATLAB0_step();
            clock_gettime( CLOCK_REALTIME, &stop);

            difference = diff(start, stop);
            accum = difference.tv_nsec;
            entire_loops = entire_loops + accum;


            fprintf(time, "%lf" "%s", accum, "\n");


            for (int j = 0; j < 12; j++) {
                fscanf(output, "%s", buff);

                if (j < 4) {
                    if ((fabs(rtY.x[j] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.x[j] != 0 && atof(buff) != 0)) {
                        error_x++;
                        //tmp++;
                        accum_error = accum_error + fabs(rtY.x[j] - atof(buff));
                        /*printf("El valor obtenido en X fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), fabs(atof(buff)*0.01));
    */				}

                } else if (j < 8) {
                    if ((fabs(rtY.z[j - 4] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.z[j - 4] != 0 && atof(buff) != 0)) {
                        error_z++;
                        //tmp++;
                        accum_error = accum_error + fabs(rtY.z[j] - atof(buff));
                        /*printf("El valor obtenido en Z fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);*/
                    }

                } else {
                    if ((fabs(rtY.u[j - 8] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.u[j - 8] != 0 && atof(buff) != 0)) {
                        error_u++;
                        //tmp++;
                        accum_error = accum_error + fabs(rtY.u[j] - atof(buff));
                        /*printf("El valor obtenido en U fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
                                "y el valor maximo permitido era de %f "
                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);*/
                        }

                }
            }
//            if (tmp != 0){
//                err++;
//                tmp = 0;
//            }
        }
//
//        printf("Errores totales en test %d: %d\n", w + 1, error_x + error_z + error_u);
//        printf("Errores en x: %d\nErrores en z: %d\nErrores en u: %d\n", error_x, error_z, error_u);
//        printf("El error acumulado fue de : %lf \n", accum_error);

        //printf( "%lf\n", accum );


        /* Close txt */
        fclose(input);
        fclose(output);

        /* Conditions to verify PC has worst case of Embedded systems */
        if (accum >= rasp3B){
            raspberry3_counter++;
        }
        if (accum >= rasp4B){
            raspberry4_counter++;
        }
        if (accum >= beaglebone){
            beaglebone_counter++;
        }

    }
    printf("En todos los loops se demora un total de : %lf segundos\n", entire_loops/1000000000);
    printf("El PC sobrepaso el maximo de la raspberry3B %d veces\n"
           "El PC sobrepaso el maximo de la raspberry4B %d veces\n"
           "El PC sobrepaso el maximo de la beaglebone %d veces\n",raspberry3_counter,raspberry4_counter,beaglebone_counter);
    printf("DONE\n");
    fclose(time);
	return 0;
}



struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

