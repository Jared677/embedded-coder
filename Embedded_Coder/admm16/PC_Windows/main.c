/*
 * main.c
 *
 *  Created on: 11-05-2022
 *      Author: Jared
 */



#include <stdio.h>

#include <stdlib.h>
#include "MATLAB0.h"
#include "rtwtypes.h"
#include<time.h>



struct timespec diff(struct timespec start, struct timespec end);


int main(int argc,char* argv[]) {

    /* Seting command line inputs */
    double tolerance;
    if(argc<3) {
        printf("\n Error en argumentos de entrada \n");
        return 0;
    }

    if(argc == 3){
        tolerance = 0.001;
    } else {
        tolerance = atof(argv[3]);
    }

    int dimension = atoi(argv[1]);
    int iterations = atoi(argv[2]);

    printf("Parametros utilizados en esta prueba: \n    Dimension del problema: %s \n    Cantidad de Iteraciones (ADMM): %s \n    Tolerancia: %s\n" ,argv[1],argv[2],argv[3]);

    /* Variable initialization */
    FILE *input;
    FILE *output;
    //FILE *time;
    FILE *cant_error;
    struct timespec start, stop;
    float Array[dimension * dimension * 2 + 1];
    char buff[dimension * dimension * 2];
    int N = 100000;
    int W = 100;
    double accum_error = 0;
    double entire_loops = 0;
    int error_x = 0;
    int error_z = 0;
    int error_u = 0;
    double accum = 0;
    int flag = 0;
    int error = 0;
    float valor = 0;


    //time = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\ADMM16\\windows_admm16.txt","w+");
    cant_error = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\ADMM16\\errores_16.txt","w+");

    /* Repetition tests LOOP */
    for (int w = 0; w < W; w++) {

        input = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\ADMM16\\input_simplify16.txt","r");
        output = fopen("C:\\Users\\Jared\\Desktop\\Memoria_pt2\\CLion\\ADMM16\\output_simplify16.txt","r");
        error_x = 0;
        error_z = 0;
        error_u = 0;


        if (input==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
            return 0;
        }
        if (output==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
            return 0;
        }
        if (time==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE TIME\n");
            return 0;
        }
        struct timespec difference;
        accum = 0;
        /* LOOP tests */
        for (int k = 0; k < N; k++) {

            for (int i = 0; i < dimension * dimension * 2 + 1; i++) {
                fscanf(input, "%s", buff);
                Array[i] = atof(buff);
            }

            MATLAB0_initialize(Array, iterations);

            clock_gettime( CLOCK_REALTIME, &start); //tic
            MATLAB0_step();
            clock_gettime( CLOCK_REALTIME, &stop); //toc

            difference = diff(start, stop);
            accum = difference.tv_nsec;
            entire_loops = entire_loops + accum;


            //fprintf(time, "%lf" "%s", accum, "\n");


            for (int j = 0; j < dimension * 3; j++) {
                fscanf(output, "%s", buff);

                if (j < dimension) {
                    if (fabs(rtY.x[j]) < 0.0001) {rtY.x[j] = 0;}
                    if (fabs(rtY.x[j] - atof(buff)) > fabs(atof(buff)*tolerance) ) {
                        error_x++;
                        flag++;
                        //accum_error = accum_error + fabs(rtY.x[j] - atof(buff));
                        //valor = atof(buff);
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.x[j]-atof(buff))/fabs(atof(buff))),",", k, "\n");

                    }

                } else if (j < dimension*2) {
                    if (fabs(rtY.z[j-dimension]) < 0.000001) {rtY.z[j-dimension] = 0;}
                    if (fabs(rtY.z[j - dimension] - atof(buff)) > fabs(atof(buff)*tolerance))  {
                        error_z++;
                        flag++;
                        //accum_error = accum_error + fabs(rtY.z[j] - atof(buff));
                        //valor = atof(buff);
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.z[j]-atof(buff))/fabs(atof(buff))),",", k, "\n");

                    }

                } else {
                    if (fabs(rtY.u[j - dimension*2]) < 0.000001) {rtY.u[j - dimension*2] = 0;}
                    if (fabs(rtY.u[j - dimension*2] - atof(buff)) > fabs(atof(buff)*tolerance) ) {
                        error_u++;
                        flag++;
                        //accum_error = accum_error + fabs(rtY.u[j] - atof(buff));
                        //valor = atof(buff);
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.u[j]-atof(buff))/fabs(atof(buff))),",", k, "\n");

                    }

                }
            }
            if (flag > 0) {
                error++;
                flag = 0;
            }
        }

        //printf("TOTAL ERROR: %d\n",error);

        /* Close txt */
        fclose(input);
        fclose(output);
        fclose(cant_error);
        if(w == 50){
            printf("50\n");
        }


    }
    printf("time: %f\n",entire_loops);
    printf("DONE\n");
    //fclose(time);
    return 0;
}



struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

