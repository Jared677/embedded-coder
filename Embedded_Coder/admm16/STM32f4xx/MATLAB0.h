/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.h
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Mon Aug 22 15:49:39 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objective: Polyspace
 * Validation result: Not run
 */

#ifndef RTW_HEADER_MATLAB0_h_
#define RTW_HEADER_MATLAB0_h_
#include <math.h>
#include <string.h>
#ifndef MATLAB0_COMMON_INCLUDES_
#define MATLAB0_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* MATLAB0_COMMON_INCLUDES_ */

/* Model Code Variants */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T P_f[256];                     /* '<Root>/P' */
  real_T q[16];                        /* '<Root>/q' */
  real_T A[256];                       /* '<Root>/A' */
  real_T b[16];                        /* '<Root>/b' */
  real_T init_x[16];                   /* '<Root>/init_x' */
  real_T init_z[16];                   /* '<Root>/init_z' */
  real_T init_u[16];                   /* '<Root>/init_u' */
  real_T rho;                          /* '<Root>/rho' */
  real_T iters;                        /* '<Root>/iters' */
} ExtU;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T x[16];                        /* '<Root>/x' */
  real_T z[16];                        /* '<Root>/z' */
  real_T u[16];                        /* '<Root>/u' */
} ExtY;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY rtY;

/* Model entry point functions */
extern void MATLAB0_initialize(float[256],int);
extern void MATLAB0_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('ADMM_static/MATLAB Function')    - opens subsystem ADMM_static/MATLAB Function
 * hilite_system('ADMM_static/MATLAB Function/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ADMM_static'
 * '<S1>'   : 'ADMM_static/MATLAB Function'
 */
#endif                                 /* RTW_HEADER_MATLAB0_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
