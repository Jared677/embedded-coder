/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Mon Aug 22 15:49:39 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objective: Polyspace
 * Validation result: Not run
 */

#include "MATLAB0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T matrix[512];
  real_T matrix_tmp[256];
  real_T matrix_tmp_0[256];
  real_T tmp[256];
  real_T rtb_u[16];
  real_T rtb_x[16];
  real_T rtb_z[16];
  real_T rtb_x_f;
  real_T rtb_z_i;
  real_T temp;
  real_T tmp_0;
  int32_T i;
  int32_T k;
  int32_T matrix_tmp_1;
  int32_T matrix_tmp_tmp;
  int32_T matrix_tmp_tmp_0;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   *  Inport: '<Root>/P'
   *  Inport: '<Root>/b'
   *  Inport: '<Root>/init_u'
   *  Inport: '<Root>/init_x'
   *  Inport: '<Root>/init_z'
   *  Inport: '<Root>/iters'
   *  Inport: '<Root>/q'
   *  Inport: '<Root>/rho'
   */
  for (i = 0; i < 16; i++) {
    rtb_x[i] = rtU.init_x[i];
    rtb_z[i] = rtU.init_z[i];
    rtb_u[i] = rtU.init_u[i];
    for (k = 0; k < 16; k++) {
      matrix_tmp_tmp_0 = (k << 4) + i;
      matrix_tmp[k + (i << 4)] = rtU.A[matrix_tmp_tmp_0];
      matrix_tmp_0[matrix_tmp_tmp_0] = 0.0;
    }
  }

  for (k = 0; k < 16; k++) {
    for (matrix_tmp_1 = 0; matrix_tmp_1 < 16; matrix_tmp_1++) {
      for (i = 0; i < 16; i++) {
        matrix_tmp_tmp_0 = k << 4;
        matrix_tmp_tmp = matrix_tmp_tmp_0 + matrix_tmp_1;
        matrix_tmp_0[matrix_tmp_tmp] += matrix_tmp[(i << 4) + matrix_tmp_1] *
          rtU.A[matrix_tmp_tmp_0 + i];
      }
    }
  }

  for (k = 0; k < 256; k++) {
    matrix[k] = rtU.rho * matrix_tmp_0[k] + rtU.P_f[k];
    matrix[k + 256] = 0.0;
  }

  for (i = 0; i < 16; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 32; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 + 1 == i + 17) {
        matrix[i + (matrix_tmp_tmp_0 << 4)] = 1.0;
      }
    }
  }

  for (i = 0; i < 16; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 != i) {
        temp = matrix[(i << 4) + matrix_tmp_tmp_0] / matrix[(i << 4) + i];
        for (k = 0; k < 32; k++) {
          matrix_tmp_1 = k << 4;
          matrix_tmp_tmp = matrix_tmp_1 + matrix_tmp_tmp_0;
          matrix[matrix_tmp_tmp] -= matrix[matrix_tmp_1 + i] * temp;
        }
      }
    }
  }

  for (i = 0; i < 16; i++) {
    temp = matrix[(i << 4) + i];
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 32; matrix_tmp_tmp_0++) {
      matrix_tmp_1 = (matrix_tmp_tmp_0 << 4) + i;
      matrix[matrix_tmp_1] /= temp;
    }
  }

  if (0 <= (int32_T)rtU.iters - 1) {
    for (k = 0; k < 256; k++) {
      tmp[k] = -rtU.A[k];
    }
  }

  for (i = 0; i < (int32_T)rtU.iters; i++) {
    for (k = 0; k < 16; k++) {
      rtb_x[k] = (rtb_z[k] - rtU.b[k]) + rtb_u[k];
    }

    for (k = 0; k < 16; k++) {
      rtb_z_i = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 16; matrix_tmp_1++) {
        rtb_z_i += matrix_tmp[(matrix_tmp_1 << 4) + k] * -rtU.rho *
          rtb_x[matrix_tmp_1];
      }

      rtb_z[k] = rtb_z_i - rtU.q[k];
    }

    for (k = 0; k < 16; k++) {
      rtb_x[k] = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 16; matrix_tmp_1++) {
        rtb_x[k] += matrix[((matrix_tmp_1 + 16) << 4) + k] * rtb_z[matrix_tmp_1];
      }
    }

    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      temp = rtb_u[matrix_tmp_tmp_0];
      rtb_z_i = 0.0;
      tmp_0 = 0.0;
      for (k = 0; k < 16; k++) {
        rtb_x_f = rtb_x[k];
        matrix_tmp_1 = (k << 4) + matrix_tmp_tmp_0;
        rtb_z_i += tmp[matrix_tmp_1] * rtb_x_f;
        tmp_0 += rtU.A[matrix_tmp_1] * rtb_x_f;
      }

      rtb_z_i = fmax(0.0, (rtb_z_i - temp) + rtU.b[matrix_tmp_tmp_0]);
      rtb_z[matrix_tmp_tmp_0] = rtb_z_i;
      rtb_u[matrix_tmp_tmp_0] = ((tmp_0 + rtb_z_i) - rtU.b[matrix_tmp_tmp_0]) +
        temp;
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* Outport: '<Root>/x' */
  memcpy(&rtY.x[0], &rtb_x[0], sizeof(real_T) << 4U);

  /* Outport: '<Root>/z' */
  memcpy(&rtY.z[0], &rtb_z[0], sizeof(real_T) << 4U);

  /* Outport: '<Root>/u' */
  memcpy(&rtY.u[0], &rtb_u[0], sizeof(real_T) << 4U);
}

/* Model initialize function */
void MATLAB0_initialize(float array[256], int iterations)
{

    for(int j = 0; j < 256*2+1; j++) {
        if (j < 256) {
            rtU.P_f[j] = array[j];
            //printf("P[%d]: %f\n",j, rtU.P_d[j]);
        } else if (j < 256*2) {
            rtU.A[j - 256] = array[j];
            //printf("A[%d]: %f\n",j-16, rtU.A[j-16]);
        } else {
            rtU.rho = array[j];

            //printf("RHO: %f\n", rtU.rho_input);
        }

    }
    rtU.iters = iterations;
    for (int j = 0; j < 16; j++) {
        rtU.q[j] = ((double)j+1.0)/4.0;
        rtU.b[j] = ((double)j+1.0)/4.0;
        rtU.init_x[j] = 0;
        rtU.init_z[j] = 0;
        rtU.init_u[j] = 0;
    }

}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
