/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Aug 20 15:42:34 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MATLAB0.h"

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T matrix[128];
  real_T matrix_tmp[64];
  real_T matrix_tmp_0[64];
  real_T tmp_1[64];
  real_T tmp[8];
  real_T tmp_0[8];
  real_T temp;
  real_T tmp_2;
  int32_T i;
  int32_T j;
  int32_T k;
  int32_T matrix_tmp_tmp;
  int32_T matrix_tmp_tmp_0;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   *  Inport: '<Root>/P'
   *  Inport: '<Root>/b'
   *  Inport: '<Root>/init_u'
   *  Inport: '<Root>/init_x'
   *  Inport: '<Root>/init_z'
   *  Inport: '<Root>/iters'
   *  Inport: '<Root>/q'
   *  Inport: '<Root>/rho'
   */
  for (i = 0; i < 8; i++) {
    rtY.x[i] = rtU.init_x[i];
    rtY.z[i] = rtU.init_z[i];
    rtY.u[i] = rtU.init_u[i];
    for (j = 0; j < 8; j++) {
      matrix_tmp_tmp_0 = (j << 3) + i;
      matrix_tmp[j + (i << 3)] = rtU.A[matrix_tmp_tmp_0];
      matrix_tmp_0[matrix_tmp_tmp_0] = 0.0;
    }
  }

  for (j = 0; j < 8; j++) {
    for (i = 0; i < 8; i++) {
      for (k = 0; k < 8; k++) {
        matrix_tmp_tmp_0 = j << 3;
        matrix_tmp_tmp = matrix_tmp_tmp_0 + i;
        matrix_tmp_0[matrix_tmp_tmp] += matrix_tmp[(k << 3) + i] *
          rtU.A[matrix_tmp_tmp_0 + k];
      }
    }
  }

  for (j = 0; j < 64; j++) {
    matrix[j] = rtU.rho * matrix_tmp_0[j] + rtU.P_f[j];
    matrix[j + 64] = 0.0;
  }

  for (i = 0; i < 8; i++) {
    for (j = 0; j < 16; j++) {
      if (j + 1 == i + 9) {
        matrix[i + (j << 3)] = 1.0;
      }
    }
  }

  for (i = 0; i < 8; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 8; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 != i) {
        j = i << 3;
        temp = matrix[j + matrix_tmp_tmp_0] / matrix[j + i];
        for (k = 0; k < 16; k++) {
          j = k << 3;
          matrix_tmp_tmp = j + matrix_tmp_tmp_0;
          matrix[matrix_tmp_tmp] -= matrix[j + i] * temp;
        }
      }
    }
  }

  for (i = 0; i < 8; i++) {
    temp = matrix[(i << 3) + i];
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      j = (matrix_tmp_tmp_0 << 3) + i;
      matrix[j] /= temp;
    }
  }

  if (0 <= (int32_T)rtU.iters - 1) {
    for (j = 0; j < 64; j++) {
      tmp_1[j] = -rtU.A[j];
    }
  }

  for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < (int32_T)rtU.iters;
       matrix_tmp_tmp_0++) {
    for (j = 0; j < 8; j++) {
      tmp[j] = (rtY.z[j] - rtU.b[j]) + rtY.u[j];
    }

    for (j = 0; j < 8; j++) {
      temp = 0.0;
      for (i = 0; i < 8; i++) {
        temp += matrix_tmp[(i << 3) + j] * -rtU.rho * tmp[i];
      }

      tmp_0[j] = temp - rtU.q[j];
    }

    for (j = 0; j < 8; j++) {
      rtY.x[j] = 0.0;
      for (i = 0; i < 8; i++) {
        rtY.x[j] += matrix[((i + 8) << 3) + j] * tmp_0[i];
      }
    }

    for (k = 0; k < 8; k++) {
      temp = 0.0;
      tmp_2 = 0.0;
      for (j = 0; j < 8; j++) {
        i = (j << 3) + k;
        temp += tmp_1[i] * rtY.x[j];
        tmp_2 += rtU.A[i] * rtY.x[j];
      }

      rtY.z[k] = fmax(0.0, (temp - rtY.u[k]) + rtU.b[k]);
      rtY.u[k] += (tmp_2 + rtY.z[k]) - rtU.b[k];
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */
}

/* Model initialize function */
void MATLAB0_initialize(float array[64], int iterations) {

    for(int j = 0; j < 64*2+1; j++) {
        if (j < 64) {
            rtU.P_f[j] = array[j];
            //printf("P[%d]: %f\n",j, rtU.P_d[j]);
        } else if (j < 64*2) {
            rtU.A[j - 64] = array[j];
            //printf("A[%d]: %f\n",j-16, rtU.A[j-16]);
        } else {
            rtU.rho = array[j];

            //printf("RHO: %f\n", rtU.rho_input);
        }

    }
    rtU.iters = iterations;

    for (int j = 0; j < 8; j++) {
        rtU.b[j] = ((double)j+1.0)/2.0;;
        rtU.q[j] = ((double)j+1.0)/2.0;;
        rtU.init_x[j] = 0;
        rtU.init_z[j] = 0;
        rtU.init_u[j] = 0;
        //printf("b[%d] = %f y q[%d] = %f \n",j,rtU.b[j],j,rtU.q[j]);
    }

}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
