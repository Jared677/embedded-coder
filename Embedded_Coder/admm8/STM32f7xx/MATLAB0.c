/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Tue Aug  9 16:53:27 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MATLAB0.h"
#include <stdio.h>

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T matrix[128];
  real_T matrix_tmp[64];
  real_T matrix_tmp_0[64];
  real_T tmp[64];
  real_T rtb_u[8];
  real_T rtb_x[8];
  real_T rtb_z[8];
  real_T rtb_x_f;
  real_T rtb_z_i;
  real_T temp;
  real_T tmp_0;
  int32_T i;
  int32_T k;
  int32_T matrix_tmp_1;
  int32_T matrix_tmp_tmp;
  int32_T matrix_tmp_tmp_0;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   *  Inport: '<Root>/P'
   *  Inport: '<Root>/b'
   *  Inport: '<Root>/init_u'
   *  Inport: '<Root>/init_x'
   *  Inport: '<Root>/init_z'
   *  Inport: '<Root>/iters'
   *  Inport: '<Root>/q'
   *  Inport: '<Root>/rho'
   */
  for (i = 0; i < 8; i++) {
    rtb_x[i] = rtU.init_x[i];
    rtb_z[i] = rtU.init_z[i];
    rtb_u[i] = rtU.init_u[i];
    for (k = 0; k < 8; k++) {
      matrix_tmp_tmp_0 = (k << 3) + i;
      matrix_tmp[k + (i << 3)] = rtU.A[matrix_tmp_tmp_0];
      matrix_tmp_0[matrix_tmp_tmp_0] = 0.0;
    }
  }

  for (k = 0; k < 8; k++) {
    for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
      for (i = 0; i < 8; i++) {
        matrix_tmp_tmp_0 = k << 3;
        matrix_tmp_tmp = matrix_tmp_tmp_0 + matrix_tmp_1;
        matrix_tmp_0[matrix_tmp_tmp] += matrix_tmp[(i << 3) + matrix_tmp_1] *
          rtU.A[matrix_tmp_tmp_0 + i];
      }
    }
  }

  for (k = 0; k < 64; k++) {
    matrix[k] = rtU.rho * matrix_tmp_0[k] + rtU.P_f[k];
    matrix[k + 64] = 0.0;
  }

  for (i = 0; i < 8; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 + 1 == i + 9) {
        matrix[i + (matrix_tmp_tmp_0 << 3)] = 1.0;
      }
    }
  }

  for (i = 0; i < 8; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 8; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 != i) {
        temp = matrix[(i << 3) + matrix_tmp_tmp_0] / matrix[(i << 3) + i];
        for (k = 0; k < 16; k++) {
          matrix_tmp_1 = k << 3;
          matrix_tmp_tmp = matrix_tmp_1 + matrix_tmp_tmp_0;
          matrix[matrix_tmp_tmp] -= matrix[matrix_tmp_1 + i] * temp;
        }
      }
    }
  }

  for (i = 0; i < 8; i++) {
    temp = matrix[(i << 3) + i];
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      matrix_tmp_1 = (matrix_tmp_tmp_0 << 3) + i;
      matrix[matrix_tmp_1] /= temp;
    }
  }

  if (0 <= (int32_T)rtU.iters - 1) {
    for (k = 0; k < 64; k++) {
      tmp[k] = -rtU.A[k];
    }
  }

  for (i = 0; i < (int32_T)rtU.iters; i++) {
    for (k = 0; k < 8; k++) {
      rtb_x[k] = (rtb_z[k] - rtU.b[k]) + rtb_u[k];
    }

    for (k = 0; k < 8; k++) {
      rtb_z_i = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
        rtb_z_i += matrix_tmp[(matrix_tmp_1 << 3) + k] * -rtU.rho *
          rtb_x[matrix_tmp_1];
      }

      rtb_z[k] = rtb_z_i - rtU.q[k];
    }

    for (k = 0; k < 8; k++) {
      rtb_x[k] = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
        rtb_x[k] += matrix[((matrix_tmp_1 + 8) << 3) + k] * rtb_z[matrix_tmp_1];
      }
    }

    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 8; matrix_tmp_tmp_0++) {
      temp = rtb_u[matrix_tmp_tmp_0];
      rtb_z_i = 0.0;
      tmp_0 = 0.0;
      for (k = 0; k < 8; k++) {
        rtb_x_f = rtb_x[k];
        matrix_tmp_1 = (k << 3) + matrix_tmp_tmp_0;
        rtb_z_i += tmp[matrix_tmp_1] * rtb_x_f;
        tmp_0 += rtU.A[matrix_tmp_1] * rtb_x_f;
      }

      rtb_z_i = fmax(0.0, (rtb_z_i - temp) + rtU.b[matrix_tmp_tmp_0]);
      rtb_z[matrix_tmp_tmp_0] = rtb_z_i;
      rtb_u[matrix_tmp_tmp_0] = ((tmp_0 + rtb_z_i) - rtU.b[matrix_tmp_tmp_0]) +
        temp;
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* Outport: '<Root>/x' */
  memcpy(&rtY.x[0], &rtb_x[0], sizeof(real_T) << 3U);

  /* Outport: '<Root>/z' */
  memcpy(&rtY.z[0], &rtb_z[0], sizeof(real_T) << 3U);

  /* Outport: '<Root>/u' */
  memcpy(&rtY.u[0], &rtb_u[0], sizeof(real_T) << 3U);
}

/* Model initialize function */
void MATLAB0_initialize(float array[64], int iterations) {
    for(int j = 0; j < 64*2+1; j++) {
        if (j < 64) {
            rtU.P_f[j] = array[j];
            //printf("P[%d]: %f\n",j, rtU.P_d[j]);
        } else if (j < 64*2) {
            rtU.A[j - 64] = array[j];
            //printf("A[%d]: %f\n",j-16, rtU.A[j-16]);
        } else {
            rtU.rho = array[j];

            //printf("RHO: %f\n", rtU.rho_input);
        }

    }
//	float P_test[64] = {1.365464,
//			-0.134305,
//			0.194266,
//			0.089020,
//			-0.180511,
//			0.024262,
//			0.283333,
//			0.093681,
//			-0.134305,
//			0.502567,
//			-0.061768,
//			-0.097004,
//			0.976412,
//			0.160442,
//			-0.143949,
//			-0.321020,
//			0.194266,
//			-0.061768,
//			0.328992,
//			0.430653,
//			0.017168,
//			-0.240918,
//			0.110508,
//			0.024728,
//			0.089020,
//			-0.097004,
//			0.430653,
//			0.370896,
//			0.407416,
//			0.504797,
//			-0.127885,
//			-0.471747,
//			-0.180511,
//			0.976412, 0.017168, 0.407416, 1.129719, -0.363643, -0.340771, 0.450939, 0.024262, 0.160442, -0.240918, 0.504797, -0.363643, 1.374229, 0.055378, -0.040764, 0.283333, -0.143949, 0.110508, -0.127885, -0.340771, 0.055378, 1.380415, -0.190895, 0.093681, -0.321020, 0.024728, -0.471747, 0.450939, -0.040764, -0.190895, 1.418727};
//	float A_test[64] = {-0.408119, -0.125896, -0.224594,
//			0.194624, 0.265931, 0.148631, 0.467014, 0.591251, -0.125896,
//			-0.188110, 0.404057, -0.088274, -0.202512, -0.025829, 0.226424, -0.658922, -0.224594, 0.404057,
//			-0.558726, -0.066183, 0.414943, -0.356007, -0.344376, 0.174693, 0.194624, -0.088274, -0.066183,
//			0.338900, 0.195274, 0.205688, 0.206025, 0.125776, 0.265931, -0.202512, 0.414943, 0.195274, -0.072375,
//			0.336231, -0.109775, 0.510838, 0.148631, -0.025829, -0.356007, 0.205688, 0.336231, 0.338932, 0.278120, -0.084420, 0.467014, 0.226424, -0.344376, 0.206025, -0.109775, 0.278120, 0.512943, -0.421659, 0.591251, -0.658922, 0.174693, 0.125776, 0.510838, -0.084420, -0.421659, -0.161764};
//	for (int k = 0; k < 64; k++) {
//		rtU.P_f[k] = P_test[k];
//		rtU.A[k] = A_test[k];
//	}
//	rtU.rho = 35.173912;
	rtU.iters = iterations;


    for (int j = 0; j < 8; j++) {
        rtU.b[j] =(double)j/2 + 0.5;
        rtU.q[j] = (double)j/2 + 0.5;
        rtU.init_x[j] = 0;
        rtU.init_z[j] = 0;
        rtU.init_u[j] = 0;
        //printf("b[%d] = %f y q[%d] = %f \n",j,rtU.b[j],j,rtU.q[j]);
    }

}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
