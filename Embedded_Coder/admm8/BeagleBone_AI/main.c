/*
 * main.c
 *
 *  Created on: 11-05-2022
 *      Author: Jared
 */



#include <stdio.h>

#include <stdlib.h>
#include "MATLAB0.h"
#include "rtwtypes.h"
#include<time.h>



struct timespec diff(struct timespec start, struct timespec end);

double rasp3B = 5973289;
double rasp4B = 2123644;
double beaglebone = 18607440;
int raspberry3_counter, raspberry4_counter, beaglebone_counter = 0;

int main(int argc,char* argv[]) {

    /* Seting command line inputs */
    double tolerance;
    if(argc<3) {
        printf("\n Error en argumentos de entrada \n");
        return 0;
    }

    if(argc == 3){
        tolerance = 0.001;
    } else {
        tolerance = atof(argv[3]);
    }

    int dimension = atoi(argv[1]);
    int iterations = atoi(argv[2]);

    printf("Parametros utilizados en esta prueba: \n    Dimension del problema: %s \n    Cantidad de Iteraciones (ADMM): %s \n    Tolerancia: %s\n" ,argv[1],argv[2],argv[3]);
    printf("%d %d %f\n",dimension,iterations,tolerance);

	/* Variable initialization */
	FILE *input;
	FILE *output;
    FILE *time;
    FILE *cant_error;
    struct timespec start, stop;
	float Array[dimension * dimension * 2 + 1];
	char buff[dimension * dimension * 2];
	int N = 100000;
    int W = 1;
    double accum_error = 0;
    double entire_loops = 0;
    int error_x = 0;
    int error_z = 0;
    int error_u = 0;
    double accum = 0;
    int flag = 0;
    int error = 0;
    float valor = 0;

    time = fopen("time.txt","w+");
    cant_error = fopen("errores.txt","w+");

    /* Repetition tests LOOP */
    for (int w = 0; w < W; w++) {

        input = fopen("input_simplify8.txt","r");
        output = fopen("output_simplify8.txt","r");
        error_x = 0;
        error_z = 0;
        error_u = 0;


        if (input==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE INPUT\n");
            return 0;
        }
        if (output==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE OUTPUT\n");
            return 0;
        }
        if (time==NULL) {
            printf("ERROR EN ASIGNACION EN EL PUNTERO DE TIME\n");
            return 0;
        }
        struct timespec difference;
        accum = 0;
        /* LOOP tests */
        for (int k = 0; k < N; k++) {

            for (int i = 0; i < dimension * dimension * 2 + 1; i++) {
                fscanf(input, "%s", buff);
                Array[i] = atof(buff);
            }

            MATLAB0_initialize(Array, iterations);

            clock_gettime( CLOCK_REALTIME, &start); //tic
            MATLAB0_step();
            clock_gettime( CLOCK_REALTIME, &stop); //toc

            difference = diff(start, stop);
            accum = difference.tv_nsec;
            entire_loops = entire_loops + accum;


            fprintf(time, "%lf" "%s", accum, "\n");


            for (int j = 0; j < dimension * 3; j++) {
                fscanf(output, "%s", buff);

                if (j < dimension) {
                    if ((fabs(rtY.x[j] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.x[j] != 0 && atof(buff) != 0)) {
                        error_x++;
                        flag++;
                        accum_error = accum_error + fabs(rtY.x[j] - atof(buff));
                        valor = atof(buff);
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.x[j]-valor)/fabs(valor)),",", k, "\n");
     //                   printf("El valor obtenido en X fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
    //                            "y el valor maximo permitido era de %f "
   //                             "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), fabs(atof(buff)*0.01));
    				}

                } else if (j < dimension*2) {
                    if ((fabs(rtY.z[j - dimension] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.z[j - dimension] != 0 && atof(buff) != 0)) {
                        error_z++;
                        flag++;
                        valor = atof(buff);
                        accum_error = accum_error + fabs(rtY.z[j] - atof(buff));
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.z[j]-valor)/fabs(valor)),",", k, "\n");
 //                       printf("El valor obtenido en Z fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
 //                               "y el valor maximo permitido era de %f "
//                                "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);
                    }

                } else {
                    if ((fabs(rtY.u[j - dimension*2] - atof(buff)) > fabs(atof(buff)*tolerance)) && (rtY.u[j - dimension*2] != 0 && atof(buff) != 0)) {
                        error_u++;
                        flag++;
                        valor = atof(buff);
                        accum_error = accum_error + fabs(rtY.u[j] - atof(buff));
                        fprintf(cant_error, "%lf" "%s" "%d" "%s", 100*(fabs(rtY.u[j]-valor)/fabs(valor)),",", k, "\n");
  //                      printf("El valor obtenido en U fue de: %f, el valor esperado es de %f\nLa diferencia de valores fue %f, "
   //                             "y el valor maximo permitido era de %f "
    //                            "\n", rtY.x[j], atof(buff),fabs(rtY.x[j] - atof(buff)), atof(buff)*0.01);
                        }

                }
            }
            if (flag > 0) {
                error++;
                flag = 0;
            }
        }

        printf("Errores totales en test %d: %d\n", w + 1, error_x + error_z + error_u);
        printf("Errores en x: %d\nErrores en z: %d\nErrores en u: %d\n", error_x, error_z, error_u);
        printf("El error acumulado fue de : %lf \n", accum_error);
        printf("TOTAL ERROR: %d\n",error);



        /* Close txt */
        fclose(input);
        fclose(output);

//        /* Conditions to verify PC performed worse than microcontrollers */
//        if (accum >= rasp3B){
//            raspberry3_counter++;
//        }
//        if (accum >= rasp4B){
//            raspberry4_counter++;
//        }
//        if (accum >= beaglebone){
//            beaglebone_counter++;
//        }

    }
//    printf("En todos los loops se demora un total de : %lf segundos\n", entire_loops/1000000000);
//    printf("El PC sobrepaso el maximo de la raspberry3B %d veces\n"
//           "El PC sobrepaso el maximo de la raspberry4B %d veces\n"
//           "El PC sobrepaso el maximo de la beaglebone %d veces\n",raspberry3_counter,raspberry4_counter,beaglebone_counter);


    printf("DONE\n");
    fclose(time);
    fclose(cant_error);
	return 0;
}



struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;

    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

