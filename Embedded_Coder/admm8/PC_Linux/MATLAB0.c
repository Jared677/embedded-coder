/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MATLAB0.c
 *
 * Code generated for Simulink model 'MATLAB0'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Tue Aug  9 16:53:27 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MATLAB0.h"
#include <stdio.h>

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void MATLAB0_step(void)
{
  real_T matrix[128];
  real_T matrix_tmp[64];
  real_T matrix_tmp_0[64];
  real_T tmp[64];
  real_T rtb_u[8];
  real_T rtb_x[8];
  real_T rtb_z[8];
  real_T rtb_x_f;
  real_T rtb_z_i;
  real_T temp;
  real_T tmp_0;
  int32_T i;
  int32_T k;
  int32_T matrix_tmp_1;
  int32_T matrix_tmp_tmp;
  int32_T matrix_tmp_tmp_0;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Inport: '<Root>/A'
   *  Inport: '<Root>/P'
   *  Inport: '<Root>/b'
   *  Inport: '<Root>/init_u'
   *  Inport: '<Root>/init_x'
   *  Inport: '<Root>/init_z'
   *  Inport: '<Root>/iters'
   *  Inport: '<Root>/q'
   *  Inport: '<Root>/rho'
   */
  for (i = 0; i < 8; i++) {
    rtb_x[i] = rtU.init_x[i];
    rtb_z[i] = rtU.init_z[i];
    rtb_u[i] = rtU.init_u[i];
    for (k = 0; k < 8; k++) {
      matrix_tmp_tmp_0 = (k << 3) + i;
      matrix_tmp[k + (i << 3)] = rtU.A[matrix_tmp_tmp_0];
      matrix_tmp_0[matrix_tmp_tmp_0] = 0.0;
    }
  }

  for (k = 0; k < 8; k++) {
    for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
      for (i = 0; i < 8; i++) {
        matrix_tmp_tmp_0 = k << 3;
        matrix_tmp_tmp = matrix_tmp_tmp_0 + matrix_tmp_1;
        matrix_tmp_0[matrix_tmp_tmp] += matrix_tmp[(i << 3) + matrix_tmp_1] *
          rtU.A[matrix_tmp_tmp_0 + i];
      }
    }
  }

  for (k = 0; k < 64; k++) {
    matrix[k] = rtU.rho * matrix_tmp_0[k] + rtU.P_f[k];
    matrix[k + 64] = 0.0;
  }

  for (i = 0; i < 8; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 + 1 == i + 9) {
        matrix[i + (matrix_tmp_tmp_0 << 3)] = 1.0;
      }
    }
  }

  for (i = 0; i < 8; i++) {
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 8; matrix_tmp_tmp_0++) {
      if (matrix_tmp_tmp_0 != i) {
        temp = matrix[(i << 3) + matrix_tmp_tmp_0] / matrix[(i << 3) + i];
        for (k = 0; k < 16; k++) {
          matrix_tmp_1 = k << 3;
          matrix_tmp_tmp = matrix_tmp_1 + matrix_tmp_tmp_0;
          matrix[matrix_tmp_tmp] -= matrix[matrix_tmp_1 + i] * temp;
        }
      }
    }
  }

  for (i = 0; i < 8; i++) {
    temp = matrix[(i << 3) + i];
    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 16; matrix_tmp_tmp_0++) {
      matrix_tmp_1 = (matrix_tmp_tmp_0 << 3) + i;
      matrix[matrix_tmp_1] /= temp;
    }
  }

  if (0 <= (int32_T)rtU.iters - 1) {
    for (k = 0; k < 64; k++) {
      tmp[k] = -rtU.A[k];
    }
  }

  for (i = 0; i < (int32_T)rtU.iters; i++) {
    for (k = 0; k < 8; k++) {
      rtb_x[k] = (rtb_z[k] - rtU.b[k]) + rtb_u[k];
    }

    for (k = 0; k < 8; k++) {
      rtb_z_i = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
        rtb_z_i += matrix_tmp[(matrix_tmp_1 << 3) + k] * -rtU.rho *
          rtb_x[matrix_tmp_1];
      }

      rtb_z[k] = rtb_z_i - rtU.q[k];
    }

    for (k = 0; k < 8; k++) {
      rtb_x[k] = 0.0;
      for (matrix_tmp_1 = 0; matrix_tmp_1 < 8; matrix_tmp_1++) {
        rtb_x[k] += matrix[((matrix_tmp_1 + 8) << 3) + k] * rtb_z[matrix_tmp_1];
      }
    }

    for (matrix_tmp_tmp_0 = 0; matrix_tmp_tmp_0 < 8; matrix_tmp_tmp_0++) {
      temp = rtb_u[matrix_tmp_tmp_0];
      rtb_z_i = 0.0;
      tmp_0 = 0.0;
      for (k = 0; k < 8; k++) {
        rtb_x_f = rtb_x[k];
        matrix_tmp_1 = (k << 3) + matrix_tmp_tmp_0;
        rtb_z_i += tmp[matrix_tmp_1] * rtb_x_f;
        tmp_0 += rtU.A[matrix_tmp_1] * rtb_x_f;
      }

      rtb_z_i = fmax(0.0, (rtb_z_i - temp) + rtU.b[matrix_tmp_tmp_0]);
      rtb_z[matrix_tmp_tmp_0] = rtb_z_i;
      rtb_u[matrix_tmp_tmp_0] = ((tmp_0 + rtb_z_i) - rtU.b[matrix_tmp_tmp_0]) +
        temp;
    }
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* Outport: '<Root>/x' */
  memcpy(&rtY.x[0], &rtb_x[0], sizeof(real_T) << 3U);

  /* Outport: '<Root>/z' */
  memcpy(&rtY.z[0], &rtb_z[0], sizeof(real_T) << 3U);

  /* Outport: '<Root>/u' */
  memcpy(&rtY.u[0], &rtb_u[0], sizeof(real_T) << 3U);
}

/* Model initialize function */
void MATLAB0_initialize(float array[64], int iterations) {

    for(int j = 0; j < 64*2+1; j++) {
        if (j < 64) {
            rtU.P_f[j] = array[j];
            //printf("P[%d]: %f\n",j, rtU.P_d[j]);
        } else if (j < 64*2) {
            rtU.A[j - 64] = array[j];
            //printf("A[%d]: %f\n",j-16, rtU.A[j-16]);
        } else {
            rtU.rho = array[j];

            //printf("RHO: %f\n", rtU.rho_input);
        }

    }
    rtU.iters = iterations;
    for (int j = 0; j < 8; j++) {
        rtU.b[j] =((double)j)/2 + 0.5;
        rtU.q[j] = ((double)j)/2 + 0.5;
        rtU.init_x[j] = 0;
        rtU.init_z[j] = 0;
        rtU.init_u[j] = 0;
        //printf("b[%d] = %f y q[%d] = %f \n",j,rtU.b[j],j,rtU.q[j]);
    }

}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
