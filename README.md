# Evaluación de herramientas de generación de código para plataformas embebidas utilizando Matlab

Repositorio en donde se encuentra el trabajo realizado a lo largo ELO308.

El contenido de este repositorio se limita a entregar un tutorial de como utilizar Embedded Coder, y como es que este código generado es implementado en diferentes plataformas embebidas. La carpeta [Tutorial](/Tutorial/) contiene los pasos a seguir para poder obtener un código en C con el uso de Embedded Coder, además incluye las configuraciones específicas que se necesitan tener para el uso correcto de una de las plataformas en la que se implementaron los códigos generados (STM32F7xx y STM32F4xx). Por otro lado en la carpeta [Embedded_coder](/Embedded_Coder/) se encuentran los códigos generados para las distintas plataformas y sistemas operativos, con los respectivos scripts encargados de caracterizar el tiempo de ejecución y comparación con las golden reference. 

Finalmente la carpeta [Matlab](/Matlab/) contiene los códigos utilizados para la comunicación y validación de resultados para las tarjetas de la familia STM32F7xx y STM32F4xx, adicionalmente, se encuentra el script encargado de generar la golden reference y códigos originales tomados como caso de estudio.
